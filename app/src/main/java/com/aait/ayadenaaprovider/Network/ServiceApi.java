package com.aait.ayadenaaprovider.Network;

import com.aait.ayadenaaprovider.Models.BaseResponse;

import com.aait.ayadenaaprovider.Models.CategoriesModel;
import com.aait.ayadenaaprovider.Models.CategoriesResponse;
import com.aait.ayadenaaprovider.Models.ConfirmCodeModel;
import com.aait.ayadenaaprovider.Models.ContactUsResponse;
import com.aait.ayadenaaprovider.Models.ForgotPasswordResponse;
import com.aait.ayadenaaprovider.Models.ListModelResponse;

import com.aait.ayadenaaprovider.Models.LoginResponse;
import com.aait.ayadenaaprovider.Models.MyDebtRespose;
import com.aait.ayadenaaprovider.Models.NewOrderResponse;
import com.aait.ayadenaaprovider.Models.NewPasswordResponse;
import com.aait.ayadenaaprovider.Models.NewTrackResponse;
import com.aait.ayadenaaprovider.Models.NewTracksResponse;
import com.aait.ayadenaaprovider.Models.NotificationResponse;
import com.aait.ayadenaaprovider.Models.OrderDetailsResponse;
import com.aait.ayadenaaprovider.Models.ProductResponse;
import com.aait.ayadenaaprovider.Models.RatesResponse;
import com.aait.ayadenaaprovider.Models.RegisterResponse;
import com.aait.ayadenaaprovider.Models.ShowProductResponse;
import com.aait.ayadenaaprovider.Models.ShowShopResponse;
import com.aait.ayadenaaprovider.Models.TermsResponse;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ServiceApi {

    @POST(Urls.Cities)
    Call<ListModelResponse> getCities(
            @Query("lang") String lang
    );
    @GET(Urls.Nations)
    Call<ListModelResponse> getNations(
            @Query("lang") String lang
    );
//    @GET(Urls.Categories)
//    Call<ListModelResponse> getCategories(
//            @Query("lang") String lang
//    );
    @GET(Urls.Categories)
    Call<CategoriesResponse> getCategory(
            @Query("lang") String lang
    );
    @GET(Urls.Terms)
    Call<TermsResponse> getTerms(
            @Query("lang") String lang
    );
    @GET(Urls.About)
    Call<TermsResponse> getAbout(
            @Query("lang") String lang
    );
    @GET(Urls.AppInfo)
    Call<ContactUsResponse> getAppInfo(
            @Query("lang") String lang
    );
    @POST(Urls.Complain)
    Call<BaseResponse> sendComplain(
            @Query("user_id") String user_id,
            @Query("lang") String lang,
            @Query("title") String title,
            @Query("body") String body
    );
    @POST(Urls.Login)
    Call<LoginResponse> Login(
            @Query("lang") String lang,
            @Query("phone") String phone,
            @Query("password") String password,
            @Query("device") String device,
            @Query("device_id") String device_id
    );
    @POST(Urls.Logout)
    Call<BaseResponse> logout(
            @Query("user_id") int user_id,
            @Query("device_id") String device_id
    );
    @Multipart
    @POST(Urls.Register)
    Call<RegisterResponse>delegateRegister(
            @Query("lang") String lang,
            @Query("type") String type,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") int nationality_id,
            @Query("civil_number") String civil_number,
            @Part MultipartBody.Part avatar,
            @Query("provider_type") String provider_type,
            @Query("shop_name") String shop_name,
            @Query("have_delegate") int have_delegate,
            @Query("delegate_number") String delegate_number,
            @Query("delegate_price") String delegate_price,
            @Query("categories_ids") String categories_ids
    );
    @Multipart
    @POST(Urls.Register)
    Call<RegisterResponse>trackRegister(
            @Query("lang") String lang,
            @Query("type") String type,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") int nationality_id,
            @Query("civil_number") String civil_number,
            @Part MultipartBody.Part avatar,
            @Query("provider_type") String provider_type,
            @Query("shop_name") String shop_name,
            @Query("have_delegate") int have_delegate,
            @Query("delegate_number") String delegate_number,
            @Query("delegate_price") String delegate_price,
            @Query("categories_ids") String categories_ids,
            @Query("person_price") String person_price,
            @Query("minimum_person") String minimum_person
    );
    @POST(Urls.ConfirmCode)
    Call<ConfirmCodeModel> confirm(
            @Query("user_id") String user_id,
            @Query("code") String code
    );
//    @GET(Urls.Offers)
//    Call<SearchResponse> getOffers(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id
//    );
//    @POST(Urls.Shops)
//    Call<ShopsResponse> getShops(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id,
//            @Query("category_id") int category_id
//    );
//    @POST(Urls.ShowShop)
//    Call<ShopDetailsResponse> showShop(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id,
//            @Query("shop_id") int shop_id
//    );
//    @POST(Urls.ProductsByCategory)
//    Call<ShopProductResponse> getProducts(
//            @Query("user_id") int user_id,
//            @Query("shop_id") int shop_id,
//            @Query("category_id") int category_id,
//            @Query("lang") String lang
//    );
    @POST(Urls.RateShop)
    Call<BaseResponse> DoRate(
            @Query("user_id") int user_id,
            @Query("shop_id") int shop_id,
            @Query("stars") int stars,
            @Query("comment") String comment
    );
    @POST(Urls.MyRates)
    Call<RatesResponse> getComments(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("shop_id") int shop_id
    );
    @POST(Urls.ShowProduct)
    Call<ShowProductResponse> showproduct(
            @Query("user_id") int user_id,
            @Query("product_id") int product_id
    );
    @POST(Urls.AddToCart)
    Call<BaseResponse> addToCart(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("product_id") int product_id,
            @Query("product_count") int product_count,
            @Query("additions") String additions
    );
//    @POST(Urls.ShowCart)
//    Call<BasketResponse> myCart(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id
//    );
    @POST(Urls.DeleteCartProduct)
    Call<BaseResponse> deleteCart(
            @Query("user_id") int user_id,
            @Query("cart_product_id") int cart_product_id
    );
    @POST(Urls.AddOrder)
    Call<BaseResponse> doOrder(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("way_type") int way_type
    );
//    @POST(Urls.ClientOrders)
//    Call<OrderResponse> getClientOrders(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id
//    );
    @POST(Urls.ChangeOrderStatus)
    Call<BaseResponse> changeStatus(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("order_id") int order_id,
            @Query("status") int status
    );
    @POST(Urls.ShowNotification)
    Call<NotificationResponse> getNotifications(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.FinshedOrders)
    Call<NewOrderResponse> getFinishedOrders(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @Multipart
    @POST(Urls.Update)
    Call<LoginResponse> clientUpdate(
            @Query("lang") String lang,
            @Query("user_id") String user_id,
            @Query("name") String name,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") int nationality_id,
            @Query("civil_number") String civil_number,
            @Part MultipartBody.Part avatar
    );
    @POST(Urls.Update)
    Call<LoginResponse> clientUpdatewithoutImage(
            @Query("lang") String lang,
            @Query("user_id") String user_id,
            @Query("name") String name,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") int nationality_id,
            @Query("civil_number") String civil_number

    );
    @POST(Urls.UpdatePassword)
    Call<BaseResponse> updatePass(
            @Query("user_id") String user_id,
            @Query("password") String password
    );
    @POST(Urls.ForgetPass)
    Call<ForgotPasswordResponse> forgetPass(
            @Query("phone") String phone
    );
    @POST(Urls.ForgetPassword)
    Call<NewPasswordResponse> forgetPassword(
            @Query("user_id") String user_id,
            @Query("code") String code
    );
    @GET(Urls.Profile)
    Call<LoginResponse> getProfile(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
//    @POST(Urls.SearchProduct)
//    Call<SearchResponse> search(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id,
//            @Query("category_id") int category_id,
//            @Query("city_id") int city_id,
//            @Query("price_list_id") int price_list_id,
//            @Query("lat") String lat,
//            @Query("lng") String lng
//    );
//    @GET(Urls.PriceList)
//    Call<PriceResponse> getPriceList(
//            @Query("lang") String lang
//    );
//    @POST(Urls.MyTracks)
//    Call<TrackResponse> getMyTracks(@Query("lang") String lang,
//                                    @Query("user_id") int user_id);
//    @GET(Urls.Tracks)
//    Call<TracksResponse> getTracks(@Query("lang") String lang);
//    @POST(Urls.BookTrack)
//    Call<BaseResponse> bookTrack(
//            @Query("lang") String lang,
//            @Query("user_id") int user_id,
//            @Query("book_day") String book_day,
//            @Query("book_time") String book_time,
//            @Query("lat") String lat,
//            @Query("lng") String lng,
//            @Query("address") String address,
//            @Query("name") String name,
//            @Query("phone") String phone,
//            @Query("category_id") String category_id,
//            @Query("persons") String persons,
//            @Query("shop_id") String shop_id,
//            @Query("type") String type
//    );
    @POST(Urls.MyDebt)
    Call<MyDebtRespose> getDebt(@Query("user_id") int user_id);
    @POST(Urls.Dept)
    Call<MyDebtRespose> Debt(@Query("user_id") int user_id);
    @Multipart
    @POST(Urls.Transfer)
    Call<BaseResponse> transfer(@Query("lang") String lang,
                                @Query("user_id") int user_id,
                                @Query("type") int type,
                                @Query("bank_name") String bank_name,
                                @Query("acc_name") String acc_name,
                                @Query("bank_number") String bank_number,
                                @Query("iban") String iban,
                                @Query("money") String money,
                                @Part MultipartBody.Part image);
    @POST(Urls.NewOrders)
    Call<NewOrderResponse> getNewOrders(@Query("user_id") int id,
                                        @Query("lang") String lang);
//    @POST(Urls.LoadingOrders)
//    Call<NewOrderResponse> getLoading(@Query("user_id") int id,
//                                      @Query("lang") String lang);
//    @POST(Urls.OnWayOrders)
//    Call<NewOrderResponse> getOnWay(@Query("user_id") int id,
//                                    @Query("lang") String lang);
    @POST(Urls.DelegateCurrentOrders)
    Call<NewOrderResponse> getCurrent(@Query("user_id") int id,
                                      @Query("lang") String lang);

    @POST(Urls.MyShop)
    Call<ShowShopResponse> getMyShop(@Query("user_id") int user_id,
                                     @Query("lang") String lang);
    @POST(Urls.ProductByCategory)
    Call<ProductResponse> getProduct(@Query("lang") String lang,
                                     @Query("user_id") int user_id,
                                     @Query("shop_id") int shop_id,
                                     @Query("category_id") int category_id);
    @POST(Urls.AddOffer)
    Call<BaseResponse> addOffer(@Query("user_id") int user_id,
                                @Query("product_id") int product_id,
                                @Query("offer_price") String offer_price);
    @POST(Urls.DeleteProduct)
    Call<BaseResponse> deleteProduct(@Query("user_id") int user_id,
                                     @Query("product_id") int product_id,
                                     @Query("lang") String lang);
    @POST(Urls.OrderDetails)
    Call<OrderDetailsResponse> getOrder(@Query("user_id") int user_id,
                                        @Query("order_id") int order_id);
    @Multipart
    @POST(Urls.AddProduct)
    Call<BaseResponse> addProduct(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("name_ar") String name_ar,
            @Query("name_en") String name_en,
            @Query("disc") String disc,
            @Query("price") String price,
            @Query("category_id") int category_id,
            @Query("additions") String additions,
            @Part List<MultipartBody.Part>images
    );
    @Multipart
    @POST(Urls.AddProduct)
    Call<BaseResponse> AddProduct(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("name_ar") String name_ar,
            @Query("name_en") String name_en,
            @Query("disc") String disc,
            @Query("price") String price,
            @Query("category_id") int category_id,
            @Part List<MultipartBody.Part>images
    );
    @Multipart
    @POST(Urls.UpdateProduct)
    Call<BaseResponse> updateProduct(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("product_id") int product_id,
            @Query("name_ar") String name_ar,
            @Query("name_en") String name_en,
            @Query("disc") String disc,
            @Query("price") String price,
            @Query("category_id") int category_id,
            @Query("additions") String additions,
            @Part List<MultipartBody.Part>images
    );
    @Multipart
    @POST(Urls.UpdateProduct)
    Call<BaseResponse> UpdateProduct(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("product_id") int product_id,
            @Query("name_ar") String name_ar,
            @Query("name_en") String name_en,
            @Query("disc") String disc,
            @Query("price") String price,
            @Query("category_id") int category_id,
            @Part List<MultipartBody.Part>images
    );

    @POST(Urls.UpdateProduct)
    Call<BaseResponse> updateProductwitoutimage(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("product_id") int product_id,
            @Query("name_ar") String name_ar,
            @Query("name_en") String name_en,
            @Query("disc") String disc,
            @Query("price") String price,
            @Query("category_id") int category_id,
            @Query("additions") String additions
    );

    @POST(Urls.UpdateProduct)
    Call<BaseResponse> UpdateProductwithoutimage(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("product_id") int product_id,
            @Query("name_ar") String name_ar,
            @Query("name_en") String name_en,
            @Query("disc") String disc,
            @Query("price") String price,
            @Query("category_id") int category_id
    );
    @POST(Urls.NewTracks)
    Call<NewTracksResponse> getNewTracks(@Query("user_id") int user_id,
                                         @Query("lang") String lang);
    @POST(Urls.TrackDatails)
    Call<NewTrackResponse> showTrack(@Query("user_id") int user_id,
                                     @Query("id") int id);
    @POST(Urls.ChangeTrackStatus)
    Call<BaseResponse> changeTrack(@Query("lang") String lang,
                                   @Query("user_id") int user_id,
                                   @Query("track_id") int track_id,
                                   @Query("status") int status);
    @POST(Urls.UpdateShop)
    Call<LoginResponse> updateShop(@Query("lang") String lang,
                                   @Query("user_id") int user_id,
                                   @Query("shop_name") String shop_name,
                                   @Query("have_delegate") int have_delegate,
                                   @Query("delegate_number") String delegate_number,
                                   @Query("delegate_price") String delegate_price,
                                   @Query("categories_ids") String categories_ids,
                                   @Query("person_price") String person_price,
                                   @Query("minimum_person") String minimum_person,
                                   @Query("active") int active);


}
