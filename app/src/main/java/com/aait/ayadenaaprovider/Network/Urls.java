package com.aait.ayadenaaprovider.Network;

public class Urls {
    public final static String baseUrl = "http://ayadenaa.aait-sa.com/api/";

    public final static String Areas = "areas";
    public final static String Cities = "cities";
    public final static String Nations = "nations";
    public final static String Categories = "categories";
    public final static String PriceList = "price-list";
    public final static String Terms = "terms";
    public final static String About = "about";
    public final static String AppInfo = "app/info";
    public final static String Register = "register";
    public final static String ConfirmCode = "check-code";
    public final static String Login = "login";
    public final static String Update = "update";
    public final static String ForgetPass = "forgot/password";
    public final static String ForgetPassword = "forgot/password/code";
    public final static String UpdatePassword = "update/password";
    public final static String Logout = "logout";
    public final static String Complain = "send/complaint";
    public final static String UpdateShop = "update/shop";
    public final static String StoreProduct = "product/store";
    public final static String SearchProduct = "product/search";
    public final static String SearchShop = "shop/search";
    public final static String ShowProduct = "product/show";
    public final static String AddToCart = "cart/add";
    public final static String ShowCart = "cart/show";
    public final static String AddOrder = "order/add-new";
    public final static String ClientOrders = "order/client";
    public final static String Offers = "product/offers";
    public final static String ShowShop = "show/shop";
    public final static String Profile = "profile";
    public final static String DeleteCartProduct = "cart/delete/product";
    public final static String UpdateCatr = "cart/update/product";
    public final static String OrderDetails = "order/provider";
    public final static String RateShop = "rate/shop";
    public final static String GetComments = "shop/comments";
    public final static String FinshedOrders = "order/finished";
    public final static String ShowNotification = "notify/show";
    public final static String DelegateNewOrders = "order/delegate/new-orders";
    public final static String ChangeOrderStatus = "order/status";
    public final static String DelegateCurrentOrders = "order/provider/current";
    public final static String DelegateOnTheWayOrders = "order/on-way";
    public final static String AllDelegates = "book/all-delegates";
    public final static String SendToDelegate = "book/send";
    public final static String ProviderNewOrders = "provider/home";
    public final static String ProviderCurrentOrders = "order/provider/current";
    public final static String AcceptOrder = "order/status";
    public final static String ShopRates = "my-rate";
    public final static String ProductsByCategory = "products/by-category";
    public final static String AddOffer = "product/add-offer";
    public final static String DeleteProduct = "product/delete";
    public final static String Banks = "banks";
    public final static String GetProduct = "product/show";
    public final static String ProviderRequests = "book/my-requests";
    public final static String AcceptBook = "book/status";
    public final static String UpdateProduct = "product/update";
    public final static String GetDept = "debt";
    public final static String Transfer = "transfer";
    public final static String Shops = "shops";
    public final static String MyTracks = "track/user";
    public final static String Tracks = "tracks";
    public final static String BookTrack = "track/booking";
    public final static String MyDebt = "wallet";
    public final static String Dept = "debt";
    public final static String LoadingOrders = "order/loading";
    public final static String OnWayOrders = "order/on-way";
    public final static String CurrentOrders = "order/delegate/current";
    public final static String MyShop = "my-shop";
    public final static String ProductByCategory = "products/by-category";
    public final static String MyRates = "my-rate";
    public final static String NewOrders = "provider/home";
    public final static String AddProduct = "product/store";
    public final static String NewTracks = "track/provider/new";
    public final static String TrackDatails = "track/show";
    public final static String ChangeTrackStatus = "track/change/status";




}
