package com.aait.ayadenaaprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaprovider.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaprovider.Models.ShopCategory;
import com.aait.ayadenaaprovider.Models.ShopCategory;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Mahmoud ibrahim on 12/2/2018.
 */

public class FamilyCategoriesAdapter extends ParentRecyclerAdapter<ShopCategory> {

    public FamilyCategoriesAdapter(final Context context, final List<ShopCategory> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        ShopCategory categoryModel = data.get(position);

        viewHolder.tvCategories.setText(categoryModel.getCategory_name());
        viewHolder.tvCategories.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View view) {
                itemClickListener.onItemClick(view, position);
            }
        });
        if (categoryModel.isChecked()) {
            viewHolder.tvCategories.setBackgroundColor(mcontext.getResources().getColor(R.color.colorWhite));
            viewHolder.tvCategories.setTextColor(mcontext.getResources().getColor(R.color.colorBlack));
        } else {
            viewHolder.tvCategories
                    .setBackgroundColor(mcontext.getResources().getColor(R.color.colorWhiteOrange));
            viewHolder.tvCategories.setTextColor(mcontext.getResources().getColor(R.color.colorWhite));
        }
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_categories)
        TextView tvCategories;

        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}
