package com.aait.ayadenaaprovider.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.Network.Urls;
import com.aait.ayadenaaprovider.R;

import com.aait.ayadenaaprovider.UI.Activities.CurrentOrderDetailsActiity;
import com.aait.ayadenaaprovider.UI.Adapters.NewOrderAdapter;
import com.aait.ayadenaaprovider.Base.BaseFragment;
import com.aait.ayadenaaprovider.Listeners.OnItemClickListener;
import com.aait.ayadenaaprovider.Listeners.PaginationAdapterCallback;
import com.aait.ayadenaaprovider.Listeners.PaginationScrollListener;
import com.aait.ayadenaaprovider.Models.OrderModel;
import com.aait.ayadenaaprovider.Models.NewOrderResponse;
import com.aait.ayadenaaprovider.UI.Adapters.ProcessedOrderAdapter;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud on 2/13/18.
 */

public class ProcessedOrderFragment extends BaseFragment implements OnItemClickListener, PaginationAdapterCallback {

    LinearLayoutManager linearLayoutManager;

    ProcessedOrderAdapter mProcessedOrderAdapter;

    List<OrderModel> mOrderModels = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;

    public static ProcessedOrderFragment newInstance() {
        Bundle args = new Bundle();
        ProcessedOrderFragment fragment = new ProcessedOrderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initializeComponents(final View view) {
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mProcessedOrderAdapter = new ProcessedOrderAdapter(mContext, mOrderModels);
        mProcessedOrderAdapter.setOnItemClickListener(this);
        mProcessedOrderAdapter.setOnPaginationClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setItemAnimator(new DefaultItemAnimator());
        rvRecycle.setAdapter(mProcessedOrderAdapter);
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

        });

        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProccessedOrder(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage());
            }
        });

        getProccessedOrder(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage());
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(final View view, final int position) {
        Intent intent = new Intent(mContext,CurrentOrderDetailsActiity.class);
        intent.putExtra("id",mOrderModels.get(position).getOrder_id()+"");
        startActivity(intent);
       // CurrentOrderDetailsActiity.startActivity((AppCompatActivity)mContext,mOrderModels.get(position));
    }

    @Override
    public void retryPageLoad() {

    }

    private void getProccessedOrder(int user_id, String lang) {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getCurrent(user_id, lang)
                .enqueue(new Callback<NewOrderResponse>() {
                    @Override
                    public void onResponse(Call<NewOrderResponse> call,
                            Response<NewOrderResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getData().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mProcessedOrderAdapter.updateAll(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<NewOrderResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}