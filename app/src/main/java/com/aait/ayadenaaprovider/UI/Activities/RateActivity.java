package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.Network.Urls;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.UI.Adapters.RateAdapter;
import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.Models.RatesResponse;
import com.aait.ayadenaaprovider.Models.RatesModel;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud on 2/17/18.
 */

public class RateActivity extends ParentActivity {

    LinearLayoutManager linearLayoutManager;

    RateAdapter mRateAdapter;

    List<RatesModel> mRateModels = new ArrayList<>();

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, RateActivity.class);
        mAppCompatActivity.startActivity(mIntent);
    }


    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.rating));
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mRateAdapter = new RateAdapter(mContext, mRateModels, R.layout.recycle_rate_row);
        rvRecycle.setAdapter(mRateAdapter);

        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRating();
            }
        });

        getRating();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_rate;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    private void getRating() {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getComments(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(), mSharedPrefManager.getUserData().getShop().getShop_id())
                .enqueue(new Callback<RatesResponse>() {
                    @Override
                    public void onResponse(Call<RatesResponse> call,
                            Response<RatesResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getData().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mRateAdapter.updateAll(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<RatesResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}
