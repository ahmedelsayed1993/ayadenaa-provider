package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.widget.TextView;

import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.Models.MyDebtRespose;
import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeptActivity extends ParentActivity {
    @BindView(R.id.my_wallet)
    TextView my_wallet;
    @Override
    protected void initializeComponents() {
        getDept();

    }
    @OnClick(R.id.pay)
    void onPayClick(){
        startActivity(new Intent(mContext,BankTransferActivity.class));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_dept;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getDept(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).Debt(mSharedPrefManager.getUserData().getUser_id()).enqueue(new Callback<MyDebtRespose>() {
            @Override
            public void onResponse(Call<MyDebtRespose> call, Response<MyDebtRespose> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        my_wallet.setText(response.body().getData()+" ");
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<MyDebtRespose> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
