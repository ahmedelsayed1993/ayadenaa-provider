package com.aait.ayadenaaprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaprovider.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaprovider.Models.ProductAdditionsModel;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Mahmoud ibrahim on 12/2/2018.
 */

public class SpecialAdditiveAdapter extends ParentRecyclerAdapter<ProductAdditionsModel> {

    public SpecialAdditiveAdapter(final Context context, final List<ProductAdditionsModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        ProductAdditionsModel specialAdditiveModel = data.get(position);
        viewHolder.tvPrice
                .setText(specialAdditiveModel.getAddition_price() + " " + mcontext.getResources().getString(R.string.SAR));
        viewHolder.tvSpecialAdditiveName.setText(specialAdditiveModel.getAddition_name());

    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_price)
        TextView tvPrice;

        @BindView(R.id.tv_special_additive_name)
        TextView tvSpecialAdditiveName;


        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}
