package com.aait.ayadenaaprovider.UI.Fragments;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaprovider.Base.BaseFragment;
import com.aait.ayadenaaprovider.Fcm.MyFirebaseInstanceIDService;
import com.aait.ayadenaaprovider.Listeners.DrawerListner;
import com.aait.ayadenaaprovider.Listeners.OnItemClickListener;
import com.aait.ayadenaaprovider.Models.BaseResponse;
import com.aait.ayadenaaprovider.Models.NavigationModel;
import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.UI.Activities.AboutUsActivity;
import com.aait.ayadenaaprovider.UI.Activities.AddProductActivity;
import com.aait.ayadenaaprovider.UI.Activities.ComplainsActivity;
import com.aait.ayadenaaprovider.UI.Activities.ContactUsActivity;
import com.aait.ayadenaaprovider.UI.Activities.DeptActivity;
import com.aait.ayadenaaprovider.UI.Activities.FamilyDetailsActivity;
import com.aait.ayadenaaprovider.UI.Activities.MyWalletActivity;
import com.aait.ayadenaaprovider.UI.Activities.NewTracksActivity;
import com.aait.ayadenaaprovider.UI.Activities.NotificationActivity;
import com.aait.ayadenaaprovider.UI.Activities.PreviousOrders;
import com.aait.ayadenaaprovider.UI.Activities.ProfileStatusActivity;
import com.aait.ayadenaaprovider.UI.Activities.SettingActivity;
import com.aait.ayadenaaprovider.UI.Activities.SplashActivity;
import com.aait.ayadenaaprovider.UI.Activities.TermsAndConditions;
import com.aait.ayadenaaprovider.UI.Adapters.NavigationDrawerAdapter;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;
import com.aait.ayadenaaprovider.Uitls.DialogUtil;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */

public class NavigationFragment extends BaseFragment implements OnItemClickListener {


    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.lay_profile)
    LinearLayout lay_profile;

    @BindView(R.id.tv_user_mobile)
    TextView tv_user_mobile;

    @BindView(R.id.civ_user_image)
    CircleImageView civ_user_image;


    ArrayList<NavigationModel> mNavigationModels;

    NavigationDrawerAdapter drawerAdapter;

    DrawerListner drawerListner;

    private AppCompatActivity activity;

    public static NavigationFragment newInstance() {
        Bundle args = new Bundle();
        NavigationFragment fragment = new NavigationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_navigation_menu;
    }

    @Override
    protected void initializeComponents(final View view) {
        activity = (AppCompatActivity) (NavigationFragment.this).getActivity();

        setNavData();
        setMenuData();
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(final View view, final int position) {

           if (mSharedPrefManager.getUserData().getShop().getShop_type().equals("normal")) {
               switch (position) {
                   case 0:
                       startActivity(new Intent(mContext, FamilyDetailsActivity.class));
                       break;
                   case 1:
                       startActivity(new Intent(mContext, ProfileStatusActivity.class));
                       break;
                   case 2:
                       startActivity(new Intent(mContext, NotificationActivity.class));
                       break;
                   case 3:
                       startActivity(new Intent(mContext, AddProductActivity.class));
                       break;
                   case 4:
                       // ContactUsActivity.startActivity((AppCompatActivity) mContext);
                       startActivity(new Intent(mContext, PreviousOrders.class));
                       break;
                   case 5:
                       // AboutAppActivity.startActivity((AppCompatActivity) mContext);
                       startActivity(new Intent(mContext, MyWalletActivity.class));
                       break;
                   case 6:
                       startActivity(new Intent(mContext, DeptActivity.class));
                       //
                       break;
                   case 7:
                       startActivity(new Intent(mContext, ComplainsActivity.class));
                       break;
                   case 8:
                       startActivity(new Intent(mContext, ContactUsActivity.class));
                       break;
                   case 9:
                       startActivity(new Intent(mContext, AboutUsActivity.class));
                       break;
                   case 10:
                       startActivity(new Intent(mContext, TermsAndConditions.class));
                       break;
                   case 11:
                       CommonUtil.ShareApp(mContext);
                       break;
                   case 12:
                       startActivity(new Intent(mContext, SettingActivity.class));
                       break;
                   case 13:
                       DialogUtil.showAlertDialog(mContext, getString(R.string.logout_description),
                               new OnClickListener() {
                                   @Override
                                   public void onClick(final DialogInterface dialogInterface, final int i) {

                                       logout();

                                   }
                               });
                       break;

               }
           }else {
               switch (position) {
                   case 0:
                       startActivity(new Intent(mContext, FamilyDetailsActivity.class));
                       break;
                   case 1:
                       startActivity(new Intent(mContext, ProfileStatusActivity.class));
                       break;
                   case 2:
                       startActivity(new Intent(mContext, NotificationActivity.class));
                       break;
                   case 3:
                       startActivity(new Intent(mContext, AddProductActivity.class));
                       break;
                   case 4:
                       // ContactUsActivity.startActivity((AppCompatActivity) mContext);
                        startActivity(new Intent(mContext, NewTracksActivity.class));
                       break;
                   case 5:
                       // AboutAppActivity.startActivity((AppCompatActivity) mContext);
                       startActivity(new Intent(mContext, PreviousOrders.class));
                       break;
                   case 6:
                       startActivity(new Intent(mContext, MyWalletActivity.class));
                       //
                       break;
                   case 7:
                       startActivity(new Intent(mContext, DeptActivity.class));
                       break;
                   case 8:
                       startActivity(new Intent(mContext, ComplainsActivity.class));
                       break;
                   case 9:
                       startActivity(new Intent(mContext, ContactUsActivity.class));
                       break;
                   case 10:
                       startActivity(new Intent(mContext, AboutUsActivity.class));
                       break;
                   case 11:
                       startActivity(new Intent(mContext, TermsAndConditions.class));
                       break;
                   case 12:
                       CommonUtil.ShareApp(mContext);
                       break;
                   case 13:
                       startActivity(new Intent(mContext, SettingActivity.class));
                       break;
                   case 14:
                       DialogUtil.showAlertDialog(mContext, getString(R.string.logout_description),
                               new OnClickListener() {
                                   @Override
                                   public void onClick(final DialogInterface dialogInterface, final int i) {

                                       logout();

                                   }
                               });
                       break;

               }
           }

        drawerListner.OpenCloseDrawer();
    }


    public void setDrawerListner(DrawerListner drawerListner) {
        this.drawerListner = drawerListner;
    }

    public void setMenuData() {
        mNavigationModels = new ArrayList<>();
        if (mSharedPrefManager.getUserData().getShop().getShop_type().equals("normal")) {
            mNavigationModels.add(new NavigationModel(getString(R.string.store), R.mipmap.store));
            mNavigationModels.add(new NavigationModel(getString(R.string.profile), R.mipmap.user));
            mNavigationModels.add(new NavigationModel(getString(R.string.Notifications), R.mipmap.alarm));
            mNavigationModels.add(new NavigationModel(getString(R.string.add_product), R.mipmap.add_product));
            mNavigationModels.add(new NavigationModel(getString(R.string.previous_orders), R.mipmap.old_orders));
            mNavigationModels.add(new NavigationModel(getString(R.string.wallet), R.mipmap.money));
            mNavigationModels.add(new NavigationModel(getString(R.string.the_indebtedness), R.mipmap.money));
            mNavigationModels.add(new NavigationModel(getString(R.string.complains), R.mipmap.call));
            mNavigationModels.add(new NavigationModel(getString(R.string.call_us), R.mipmap.complaint));
            mNavigationModels.add(new NavigationModel(getString(R.string.about_app), R.mipmap.about));
            mNavigationModels.add(new NavigationModel(getString(R.string.terms_and_conditions), R.mipmap.terms));
            mNavigationModels.add(new NavigationModel(getString(R.string.share_app), R.mipmap.sharee));
            mNavigationModels.add(new NavigationModel(getString(R.string.setting), R.mipmap.setting));
            mNavigationModels.add(new NavigationModel(getString(R.string.logout), R.mipmap.sign_out));
        }else if (mSharedPrefManager.getUserData().getShop().getShop_type().equals("track")){
            mNavigationModels.add(new NavigationModel(getString(R.string.store), R.mipmap.store));
            mNavigationModels.add(new NavigationModel(getString(R.string.profile), R.mipmap.user));
            mNavigationModels.add(new NavigationModel(getString(R.string.Notifications), R.mipmap.alarm));
            mNavigationModels.add(new NavigationModel(getString(R.string.add_product), R.mipmap.add_product));
            mNavigationModels.add(new NavigationModel(getString(R.string.track_orders),R.mipmap.logo_black));
            mNavigationModels.add(new NavigationModel(getString(R.string.previous_orders), R.mipmap.old_orders));
            mNavigationModels.add(new NavigationModel(getString(R.string.wallet), R.mipmap.money));
            mNavigationModels.add(new NavigationModel(getString(R.string.the_indebtedness), R.mipmap.money));
            mNavigationModels.add(new NavigationModel(getString(R.string.complains), R.mipmap.call));
            mNavigationModels.add(new NavigationModel(getString(R.string.call_us), R.mipmap.complaint));
            mNavigationModels.add(new NavigationModel(getString(R.string.about_app), R.mipmap.about));
            mNavigationModels.add(new NavigationModel(getString(R.string.terms_and_conditions), R.mipmap.terms));
            mNavigationModels.add(new NavigationModel(getString(R.string.share_app), R.mipmap.sharee));
            mNavigationModels.add(new NavigationModel(getString(R.string.setting), R.mipmap.setting));
            mNavigationModels.add(new NavigationModel(getString(R.string.logout), R.mipmap.sign_out));
        }


        rvRecycle.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        drawerAdapter = new NavigationDrawerAdapter(mContext, mNavigationModels,
                R.layout.recycle_navigation_row);
        drawerAdapter.setOnItemClickListener(this);
        rvRecycle.setAdapter(drawerAdapter);
    }


    public void setNavData() {
        if (mSharedPrefManager.getLoginStatus()) {
            tv_user_mobile.setText(mSharedPrefManager.getUserData().getName());
            Glide.with(mContext).load(mSharedPrefManager.getUserData().getAvatar()).asBitmap()
                    .placeholder(R.mipmap.logo).error(R.mipmap.logo).into(civ_user_image);
        }else {
            tv_user_mobile.setText("");

        }
    }
 private void logout(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).logout(mSharedPrefManager.getUserData().getUser_id(), MyFirebaseInstanceIDService.getToken(mContext)).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mSharedPrefManager.Logout();
                        startActivity(new Intent(mContext, SplashActivity.class));
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
