package com.aait.ayadenaaprovider.UI.Activities;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.Models.BaseResponse;
import com.aait.ayadenaaprovider.Models.NewTrackModel;
import com.aait.ayadenaaprovider.Models.NewTrackResponse;
import com.aait.ayadenaaprovider.Models.OrderModel;
import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentTrackDetailsActivity extends ParentActivity {
    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;



    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;


    @BindView(R.id.iv_client_image)
    CircleImageView ivClientImage;

    @BindView(R.id.tv_client_name)
    TextView tvClientName;

    @BindView(R.id.tv_client_city)
    TextView tvClientCity;




    @BindView(R.id.iv_step_two)
    ImageView iv_step_two;
    @BindView(R.id.tv_step_two)
    TextView tv_step_two;
    @BindView(R.id.iv_step_three)
    ImageView iv_step_three;
    @BindView(R.id.tv_step_three)
    TextView tv_step_three;
    @BindView(R.id.tv_price)
    TextView tv_price;


    @BindView(R.id.iv_order_status)
    ImageView iv_order_status;

    @BindView(R.id.tv_total_coast)
    TextView tv_total_coast;
    @BindView(R.id.btn_accept)
    Button btn_accept;
    @BindView(R.id.btn_accept1)
    Button btn_accept1;
    @BindView(R.id.btn_accept2)
    Button btn_accept2;
    @BindView(R.id.btn_accept3)
    Button btn_accept3;
    @BindView(R.id.btn_accept4)
    Button btn_accept4;
    String id;
    @Override
    protected void initializeComponents() {
        id = getIntent().getStringExtra("id");
        getTrack();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_current_track_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    void setOrderData(NewTrackModel orderData) {

        Glide.with(mContext).load(orderData.getUser_image()).asBitmap()
                .into(ivClientImage);

        tvOrderTime
                .setText(orderData.getCreated_at() + " ");
        tvOrderNumber
                .setText(orderData.getId() + "");

        tvClientName.setText(orderData.getUser_name());


        tvClientCity.setText(orderData.getAddress());
        tv_total_coast.setText(orderData.getPrice()+" "+mContext.getResources().getString(R.string.SAR));
        tv_price.setText(orderData.getPersons()+"");




            if (orderData.getStatus()>= 3) {
                btn_accept.setVisibility(View.GONE);
                iv_step_two.setImageResource(R.mipmap.step_two);
                tv_step_two.setTextColor(mContext.getResources().getColor(R.color.order_step_2_color));
                iv_order_status.setImageResource(R.mipmap.order_two);

            }
            if (orderData.getStatus() >= 4) {
                btn_accept.setVisibility(View.GONE);
                iv_step_three.setImageResource(R.mipmap.step_three);
                tv_step_three.setTextColor(mContext.getResources().getColor(R.color.order_step_3_color));
                iv_order_status.setImageResource(R.mipmap.order_three);

            }

            if (orderData.getStatus() == 1) {
                btn_accept2.setVisibility(View.VISIBLE);
            }
            if (orderData.getStatus() == 3) {
                btn_accept3.setVisibility(View.VISIBLE);
            }
        if (orderData.getStatus() == 4) {
            btn_accept4.setVisibility(View.VISIBLE);
        }

    }

    @OnClick(R.id.btn_accept2)
    void onAccept2(){
        changeStatus(2,getString(R.string.order_completed));
    }
    @OnClick(R.id.btn_accept3)
    void onAccept3(){
        changeStatus(3,getString(R.string.delegate_on_way));
    }
    @OnClick(R.id.btn_accept4)
    void onAccept4(){
        changeStatus(4,getString(R.string.client_recieve_oder));
    }
    private void getTrack(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).showTrack(mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(id)).enqueue(new Callback<NewTrackResponse>() {
            @Override
            public void onResponse(Call<NewTrackResponse> call, Response<NewTrackResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        setOrderData(response.body().getData());
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<NewTrackResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void changeStatus(int status, final String message){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeTrack(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(id),status).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,message);
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
