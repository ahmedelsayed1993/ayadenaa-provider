package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.support.v7.widget.SwitchCompat;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */

public class SettingActivity extends ParentActivity implements OnMenuItemClickListener {


    @BindView(R.id.tv_app_language_title)
    TextView tvAppLanguageTitle;

    @BindView(R.id.tv_app_language_desc)
    TextView tvAppLanguageDesc;

    @BindView(R.id.tv_app_notification_title)
    TextView tvAppNotificationTitle;

    @BindView(R.id.tv_app_notification_desc)
    TextView tvAppNotificationDesc;

    @BindView(R.id.tv_lang)
    TextView tv_lang;

    @BindView(R.id.switch_notification)
    SwitchCompat switchNotification;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, SettingActivity.class);
        mAppCompatActivity.startActivity(mIntent);
    }


    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.setting));
        setLanguageData();
        switchNotification.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                mSharedPrefManager.setNotificationStatus(b);
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_setting;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }


    @OnClick(R.id.tv_lang)
    void onLangClick() {
        PopupMenu popup = new PopupMenu(this, tv_lang);
        popup.setOnMenuItemClickListener(this);// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.lang_popup, popup.getMenu());
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.lang_arabic:
                mLanguagePrefManager.setAppLanguage("ar");
                startActivity(new Intent(mContext,SplashActivity.class));
                return true;
            case R.id.lang_english:
                mLanguagePrefManager.setAppLanguage("en");
                startActivity(new Intent(mContext,SplashActivity.class));
                return true;
            default:
                return false;
        }
    }


    void setLanguageData() {
        if (mLanguagePrefManager.getAppLanguage().equals("en")) {
            tv_lang.setText(R.string.english);
        } else {
            tv_lang.setText(R.string.arabic);
        }

        if (mSharedPrefManager.getNotificationStatus()){
            switchNotification.setChecked(true);
        }else {
            switchNotification.setChecked(false);
        }
    }




}
