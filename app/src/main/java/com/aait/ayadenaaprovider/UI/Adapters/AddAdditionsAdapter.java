package com.aait.ayadenaaprovider.UI.Adapters;


import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.aait.ayadenaaprovider.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaprovider.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaprovider.Models.AddAdditiveModel;
import com.aait.ayadenaaprovider.R;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Mahmoud ibrahim on 12/2/2018.
 */

public class AddAdditionsAdapter extends ParentRecyclerAdapter<AddAdditiveModel> {

    public AddAdditionsAdapter(final Context context, final List<AddAdditiveModel> data) {
        super(context, data);
    }


    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        View viewItem = inflater.inflate(R.layout.recycler_special, parent, false);
        viewHolder = new ListAdapter(viewItem);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ListAdapter listAdapter = (ListAdapter) holder;
        AddAdditiveModel additionModel = data.get(position);


        listAdapter.special_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {

                data.get(position).setName_ar(charSequence.toString());

            }

            @Override
            public void afterTextChanged(final Editable editable) {
            }
        });

        listAdapter.ed_order_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {
                data.get(position).setName_en(charSequence.toString());

            }

            @Override
            public void afterTextChanged(final Editable editable) {
            }
        });
        listAdapter.iv_add.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.get(position).setPrice(Integer.parseInt(s.toString()));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    protected class ListAdapter extends ParentRecyclerViewHolder {

        @BindView(R.id.add_ar)
        EditText special_name;



        @BindView(R.id.add_en)
        EditText ed_order_number;
        @BindView(R.id.add_price)
        EditText iv_add;

        public ListAdapter(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }
}
