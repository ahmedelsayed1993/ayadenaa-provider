package com.aait.ayadenaaprovider.UI.Activities;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.ayadenaaprovider.App.Constant.BundleData;
import com.aait.ayadenaaprovider.Listeners.DiscountListner;
import com.aait.ayadenaaprovider.Models.ShowProductResponse;
import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.Network.Urls;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.UI.Adapters.SpecialAdditiveAdapter;
import com.aait.ayadenaaprovider.UI.Fragments.DiscountProductDialogFragment;
import com.aait.ayadenaaprovider.Base.ParentActivity;

import com.aait.ayadenaaprovider.Listeners.OnItemClickListener;
import com.aait.ayadenaaprovider.Models.BaseResponse;
import com.aait.ayadenaaprovider.Models.ProductAdditionsModel;
import com.aait.ayadenaaprovider.Models.ProductModel;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;
import com.aait.ayadenaaprovider.Uitls.DialogUtil;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud on 2/17/18.
 */

public class ProductDetailsActivity extends ParentActivity implements OnItemClickListener, DiscountListner {

    @BindView(R.id.iv_image)
    ImageView ivImage;

    @BindView(R.id.iv_has_offer)
    ImageView iv_has_offer;

    @BindView(R.id.tv_food_name)
    TextView tvFoodName;

    @BindView(R.id.tv_normal_price)
    TextView tvNormalPrice;

    @BindView(R.id.tv_discount_price)
    TextView tvDiscountPrice;

    @BindView(R.id.tv_description)
    TextView tvDescription;


    @BindView(R.id.rv_Recycle)
    RecyclerView rvRecycle;

    LinearLayoutManager linearLayoutManager;

    SpecialAdditiveAdapter mSpecialAdditiveAdapter;

    List<ProductAdditionsModel> mSpecialAdditiveModels = new ArrayList<>();

    ProductModel mFoodModel;
    String id;

    public static void startActivity(AppCompatActivity mAppCompatActivity, int foodModel) {
        Intent mIntent = new Intent(mAppCompatActivity, ProductDetailsActivity.class);
        mIntent.putExtra(BundleData.FOOD_MODEL, foodModel);
        mAppCompatActivity.startActivity(mIntent);
    }

    @Override
    public void discountOf(final String discunt) {
        addDiscunt(mSharedPrefManager.getUserData().getUser_id(),
                 Integer.parseInt(id), discunt);
    }


    @Override
    protected void initializeComponents() {

        id = getIntent().getStringExtra("id");



        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mSpecialAdditiveAdapter = new SpecialAdditiveAdapter(mContext, mSpecialAdditiveModels,
                R.layout.recycle_special_adictive);
        mSpecialAdditiveAdapter.setOnItemClickListener(this);
        rvRecycle.setAdapter(mSpecialAdditiveAdapter);
        showProduct(mSharedPrefManager.getUserData().getUser_id());

        createOptionsMenu(R.menu.product_menu);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_item;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.product_edit:
                Intent intent = new Intent(mContext,EditProductActivity.class);
                intent.putExtra("id",id);
                startActivity(intent);
                break;
            case R.id.product_discount:

                showDiscountProduct(getSupportFragmentManager());
                return true;
            case R.id.product_delete:
                DialogUtil.showAlertDialog(mContext, getString(R.string.delete_the_product),
                        new OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialogInterface, final int i) {
                                deleteProduct(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage(),Integer.parseInt(id));
                            }
                        });
                return true;
            default:
                onBackPressed();
                break;
        }
        return true;
    }

    void showDiscountProduct(FragmentManager mFragmentManager) {
        DiscountProductDialogFragment batteryDetailsDialogFragment = DiscountProductDialogFragment
                .newInstance(mFoodModel);
        batteryDetailsDialogFragment.show(mFragmentManager, "EditProduct");
        batteryDetailsDialogFragment.setListner(this);
    }

    @Override
    public void onItemClick(final View view, final int position) {
    }

    void setData(ProductModel mFoodModel) {
        setToolbarTitle(mFoodModel.getProduct_name());
        Glide.with(mContext).load(mFoodModel.getProduct_image()).asBitmap().placeholder(R.mipmap.splash)
                .into(ivImage);
        tvFoodName.setText(mFoodModel.getProduct_name());
        tvDescription.setText(mFoodModel.getProduct_disc());
        tvNormalPrice.setText(mFoodModel.getProduct_price() + " " + mContext.getResources().getString(R.string.SAR));

        if (mFoodModel.isProduct_have_offer()==false) {
            tvDiscountPrice.setVisibility(View.GONE);
            iv_has_offer.setVisibility(View.GONE);
        } else {
            CommonUtil.setStrokInText(tvNormalPrice);
            tvDiscountPrice
                    .setText(mFoodModel.getProduct_offer() + " " + mContext.getResources().getString(R.string.SAR));
            iv_has_offer.setVisibility(View.VISIBLE);
        }

    }
    private void showProduct(int user_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).showproduct(user_id,Integer.parseInt(id)).enqueue(new Callback<ShowProductResponse>() {
            @Override
            public void onResponse(Call<ShowProductResponse> call, Response<ShowProductResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        setData(response.body().getData());
                        mFoodModel = response.body().getData();
                        mSpecialAdditiveAdapter.updateAll(response.body().getData().getProduct_additions());
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ShowProductResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void addDiscunt(int user_id,  int product_id, String discount) {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class)
                .addOffer(user_id,  product_id, discount)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call,
                            Response<BaseResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()==1) {
                                CommonUtil.makeToast(mContext, response.body().getMsg());
                                MainActivity.startActivity((AppCompatActivity) mContext);
                            } else {
                                CommonUtil.makeToast(mContext, response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                });
    }

    private void deleteProduct(int user_id, String lang, int product_id) {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class)
                .deleteProduct(user_id, product_id, lang)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call,
                            Response<BaseResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()==1) {
                                CommonUtil.makeToast(mContext, response.body().getMsg());
                                MainActivity.startActivity((AppCompatActivity) mContext);
                            } else {
                                CommonUtil.makeToast(mContext, response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                });
    }
}
