package com.aait.ayadenaaprovider.UI.Activities;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.Listeners.OnItemClickListener;
import com.aait.ayadenaaprovider.Models.CategoriesModel;
import com.aait.ayadenaaprovider.Models.CategoriesResponse;
import com.aait.ayadenaaprovider.Models.LoginResponse;
import com.aait.ayadenaaprovider.Models.ShowShopModel;
import com.aait.ayadenaaprovider.Models.ShowShopResponse;
import com.aait.ayadenaaprovider.Models.UserModel;
import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.UI.Adapters.CategoriesAdapter;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;
import com.aait.ayadenaaprovider.Uitls.ValidationUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreProfileActivity extends ParentActivity implements OnItemClickListener {
    @BindView(R.id.category_recycler)
    RecyclerView category_recycler;
    LinearLayoutManager linearLayoutManager;
    ArrayList<CategoriesModel> categoriesModels = new ArrayList<>();
    CategoriesAdapter categoriesAdapter;
    ArrayList<Integer> ids = new ArrayList<>();
    @BindView(R.id.myRadioGroup)
    RadioGroup myRadioGroup;
    @BindView(R.id.til_store_name)
    TextInputLayout tilStoreName;

    @BindView(R.id.et_store_name)
    TextInputEditText etStoreName;
    @BindView(R.id.radio_yes)
    RadioButton radioYes;

    @BindView(R.id.radio_no)
    RadioButton radioNo;


    @BindView(R.id.til_delivery_phone)
    TextInputLayout tilDeliveryPhone;

    @BindView(R.id.et_delivery_phone)
    TextInputEditText etDeliveryPhone;

    @BindView(R.id.til_delivry_cost)
    TextInputLayout tilDelivryCost;

    @BindView(R.id.et_delivery_cost)
    TextInputEditText etDeliveryCost;

    @BindView(R.id.lay_show_has_delivery)
    LinearLayout layShowHasDelivery;

    String type;
    String categories;
    boolean isHasDelivery = false;
    @Override
    protected void initializeComponents() {
        getShop();
        radioYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                isHasDelivery = b;
                if (b) {
                    layShowHasDelivery.setVisibility(View.VISIBLE);
                } else {
                    layShowHasDelivery.setVisibility(View.GONE);
                }
            }
        });

        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        categoriesAdapter = new CategoriesAdapter(mContext,categoriesModels,R.layout.recycler_category);
        categoriesAdapter.setOnItemClickListener(this);
        category_recycler.setLayoutManager(linearLayoutManager);
        category_recycler.setAdapter(categoriesAdapter);
        getCategories();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_store_profile;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId()==R.id.cat){

            if (categoriesModels.get(position).isChecked()) {
                categoriesModels.get(position).setChecked(false);
            } else {
                categoriesModels.get(position).setChecked(true);
            }
            categoriesAdapter.notifyDataSetChanged();

            for (CategoriesModel categoryModel : categoriesModels) {
                if (categoryModel.isChecked()) {
                    ids.add(categoryModel.getId());
                }
            }
            int i = 0;

            String id = "";
            for (CategoriesModel categoryModel : categoriesModels) {
                i++;
                if (categoryModel.isChecked()) {
                    if (i != categoriesModels.size()) {
                        id = id+categoryModel.getId()+",";

                    } else {
                        id = id + categoryModel.getId() + "";

                    }
                }
            }
            if (id != null && id.length()>0 && id.endsWith(",")){
                id = id.replace(id.substring(id.length()-1),"");
            }
            categories = id;
            Log.e("id",categories);
        }

    }
    private void getCategories(){
       // showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCategory(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
               // hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().size()==0){
                            category_recycler.setVisibility(View.GONE);
                        }else {
                            categoriesAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
               // hideProgressDialog();

            }
        });
    }
    private void setUserData(ShowShopModel showShopModel) {

        etStoreName.setText(showShopModel.getShop_name());
        etDeliveryCost.setText(showShopModel.getShop_delegate_price()+ "");
        etDeliveryPhone.setText(showShopModel.getShop_delegate_phone() + "");
        if (showShopModel.isShop_have_delegate()==true) {
            radioYes.setChecked(true);
            layShowHasDelivery.setVisibility(View.VISIBLE);
        } else {
            radioNo.setChecked(true);
        }

    }
    private void getShop(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getMyShop(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ShowShopResponse>() {
            @Override
            public void onResponse(Call<ShowShopResponse> call, Response<ShowShopResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        setUserData(response.body().getData());

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ShowShopResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.btn_register)
    void onUpdate(){
        if (isHasDelivery) {
            if (registerValidateWithDelivery()) {
                updateStore(mLanguagePrefManager.getAppLanguage(),
                        etStoreName.getText().toString(), 1,
                        etDeliveryCost.getText().toString(), etDeliveryPhone.getText().toString(), categories);
            }
        } else {
            if
                    (registerValidation()) {
                updateStore(mLanguagePrefManager.getAppLanguage(),
                        etStoreName.getText().toString(), 0,
                        "", "", categories);
            }
        }
    }

    boolean registerValidation() {
        if (!ValidationUtils.checkError(etStoreName, tilStoreName, getString(R.string.fill_empty))) {
            return false;
        } else if (categories==null) {
            CommonUtil.makeToast(mContext,getString(R.string.choose_category));
            return false;
        }
        return true;
    }

    boolean registerValidateWithDelivery() {
        if (!ValidationUtils.checkError(etStoreName, tilStoreName, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etDeliveryPhone, tilDeliveryPhone, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etDeliveryCost, tilDelivryCost, getString(R.string.fill_empty))) {
            return false;
        }else if (categories==null) {
            CommonUtil.makeToast(mContext,getString(R.string.choose_category));
            return false;
        }
        return true;
    }
    private void updateStore(String lang, String storeName, int haveMandob, String delegatePrice,
                             String delegatePhone, String activities) {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class)
                .updateShop(lang,mSharedPrefManager.getUserData().getUser_id(), storeName, haveMandob, delegatePhone, delegatePrice, activities, mSharedPrefManager.getUserData().getShop().getShop_person_price(),mSharedPrefManager.getUserData().getShop().getShop_minimum_person(),1)
                .enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call,
                                           Response<LoginResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()==1) {
                                mSharedPrefManager.setUserData(response.body().getData());

                                CommonUtil.makeToast(mContext, getString(R.string.profile_update_succ));
                                onBackPressed();
                            } else {
                                CommonUtil.makeToast(mContext, response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                });
    }


}
