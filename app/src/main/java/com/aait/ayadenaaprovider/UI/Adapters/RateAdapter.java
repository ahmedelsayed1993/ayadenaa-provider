package com.aait.ayadenaaprovider.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaprovider.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaprovider.Models.RatesModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Mahmoud ibrahim on 12/2/2018.
 */

public class RateAdapter extends ParentRecyclerAdapter<RatesModel> {

    public RateAdapter(final Context context, final List<RatesModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        RatesModel rateModel = data.get(position);
        Glide.with(mcontext).load(rateModel.getComment_user_img()).asBitmap().skipMemoryCache(true)
                .placeholder(R.mipmap.splash)
                .into(viewHolder.civImage);
        viewHolder.tvName.setText(rateModel.getComment_username());
        viewHolder.tvRate.setText(rateModel.getComment());
        viewHolder.tvDate.setText(rateModel.getComment_date());

    }


    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.civ_image)
        CircleImageView civImage;

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.tv_date)
        TextView tvDate;

        @BindView(R.id.tv_rate)
        TextView tvRate;


        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}
