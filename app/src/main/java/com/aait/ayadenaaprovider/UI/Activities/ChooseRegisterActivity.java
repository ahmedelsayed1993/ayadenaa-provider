package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;

import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.R;

import butterknife.OnClick;

public class ChooseRegisterActivity extends ParentActivity {
    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_choose_register;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.family)
    void onFamilyClick(){
        Intent intent = new Intent(mContext,RegisterActivity.class);
        intent.putExtra("type","1");
        startActivity(intent);
    }
    @OnClick(R.id.track)
    void onTrackClick(){
        Intent intent = new Intent(mContext,RegisterActivity.class);
        intent.putExtra("type","2");
        startActivity(intent);
    }
}
