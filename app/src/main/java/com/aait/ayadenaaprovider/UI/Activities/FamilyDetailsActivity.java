package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.ayadenaaprovider.Models.ProductResponse;
import com.aait.ayadenaaprovider.Models.ShowShopModel;
import com.aait.ayadenaaprovider.Models.ShowShopResponse;
import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.Network.Urls;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.UI.Adapters.FamilyCategoriesAdapter;
import com.aait.ayadenaaprovider.UI.Adapters.OffersAdapter;
import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.Listeners.OnItemClickListener;
import com.aait.ayadenaaprovider.Models.ShopCategory;

import com.aait.ayadenaaprovider.Models.ShopProductModel;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud on 2/17/18.
 */

public class FamilyDetailsActivity extends ParentActivity implements OnItemClickListener {

    @BindView(R.id.tv_family_name)
    TextView tvFamilyName;

    @BindView(R.id.tv_city)
    TextView tvCity;

    @BindView(R.id.rb_rating)
    AppCompatRatingBar rbRating;

    @BindView(R.id.rv_recycle_categores)
    RecyclerView rvRecycleCategores;



    @BindView(R.id.civ_image)
    CircleImageView civ_image;

    @BindView(R.id.iv_background)
    ImageView Background;

    @BindView(R.id.btn_rate)
    Button btnRate;

    LinearLayoutManager linearLayoutManager;

    FamilyCategoriesAdapter mFamilyCategoriesAdapter;

    List<ShopCategory> mCategoryModels = new ArrayList<>();


    LinearLayoutManager linearLayoutManagerVertical;

    OffersAdapter mOffersAdapter;

    List<ShopProductModel> mFoodModels = new ArrayList<>();


    int selectedPosition;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, FamilyDetailsActivity.class);
        mAppCompatActivity.startActivity(mIntent);
    }


    @Override
    protected void initializeComponents() {
        setToolbarTitle("");



        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvRecycleCategores.setLayoutManager(linearLayoutManager);
        mFamilyCategoriesAdapter = new FamilyCategoriesAdapter(mContext, mCategoryModels,
                R.layout.recycle_family_categories_row);
        mFamilyCategoriesAdapter.setOnItemClickListener(this);
        rvRecycleCategores.setAdapter(mFamilyCategoriesAdapter);

        linearLayoutManagerVertical = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManagerVertical);
        mOffersAdapter = new OffersAdapter(mContext, mFoodModels);
        mOffersAdapter.setOnItemClickListener(this);
        rvRecycle.setAdapter(mOffersAdapter);
        getShop();

        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mCategoryModels.size() != 0) {
                    mCategoryModels.get(0).setChecked(true);
                    getProductCategories(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),mSharedPrefManager.getUserData().getShop().getShop_id(),mCategoryModels.get(0).getCategory_id());

                }
            }
        });

        if (mCategoryModels.size() != 0) {
            mCategoryModels.get(0).setChecked(true);
            getProductCategories(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),mSharedPrefManager.getUserData().getShop().getShop_id(),mCategoryModels.get(0).getCategory_id());

        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_family_store_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @OnClick(R.id.btn_rate)
    void onRateClick() {
        RateActivity.startActivity((AppCompatActivity) mContext);
    }

    @Override
    public void onItemClick(final View view, final int position) {
        if (view.getId() == R.id.tv_categories) {
            // click on categories section
            CommonUtil.PrintLogE("categories click");
            selectedPosition = position;
            if (!mCategoryModels.get(selectedPosition).isChecked()) {
                for (int i = 0; i < mCategoryModels.size(); i++) {
                    mCategoryModels.get(i).setChecked(false);
                }

                mCategoryModels.get(selectedPosition).setChecked(true);
                mFamilyCategoriesAdapter.notifyDataSetChanged();

                getProductCategories(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),mSharedPrefManager.getUserData().getShop().getShop_id(),mCategoryModels.get(position).getCategory_id());
            } else {
                CommonUtil.PrintLogE("Selected before .... :) (:");
            }
        } else {
            // click on food item
            Intent intent = new Intent(mContext,ProductDetailsActivity.class);
            intent.putExtra("id",mFoodModels.get(position).getProduct_id()+"");
            startActivity(intent);
        }
    }


    private void getProductCategories(String lang, int user_id, int shop_id, int category_id) {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getProduct(lang, user_id, shop_id, category_id)
                .enqueue(new Callback<ProductResponse>() {
                    @Override
                    public void onResponse(Call<ProductResponse> call,
                            Response<ProductResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getData().getProducts().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mOffersAdapter.updateAll(response.body().getData().getProducts());
                                mOffersAdapter.updateAll(response.body().getData().getProducts());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }

    void setData(ShowShopModel showShopModel) {
        mCategoryModels = showShopModel.getUser_categories();
        mCategoryModels.get(0).setChecked(true);
        Glide.with(mContext).load(showShopModel.getUser_image()).asBitmap()
                .placeholder(R.mipmap.splash)
                .into(civ_image);
        Glide.with(mContext).load(showShopModel.getUser_image()).asBitmap()
                .placeholder(R.mipmap.splash)
                .into(Background);
        tvFamilyName.setText(showShopModel.getShop_name());
        tvCity.setText(
                showShopModel.getUser_city()
                        );
    }
    private void getShop(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getMyShop(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ShowShopResponse>() {
            @Override
            public void onResponse(Call<ShowShopResponse> call, Response<ShowShopResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        setData(response.body().getData());
                        mFamilyCategoriesAdapter.updateAll(response.body().getData().getUser_categories());
                        getProductCategories(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),mSharedPrefManager.getUserData().getShop().getShop_id(),response.body().getData().getUser_categories().get(0).getCategory_id());

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ShowShopResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

}
