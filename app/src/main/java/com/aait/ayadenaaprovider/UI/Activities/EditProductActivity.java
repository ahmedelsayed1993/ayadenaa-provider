package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.Listeners.OnItemClickListener;
import com.aait.ayadenaaprovider.Models.AddAdditiveModel;
import com.aait.ayadenaaprovider.Models.BaseResponse;
import com.aait.ayadenaaprovider.Models.ProductModel;
import com.aait.ayadenaaprovider.Models.ShopCategories;
import com.aait.ayadenaaprovider.Models.ShowProductResponse;
import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.UI.Adapters.AddAdditionsAdapter;
import com.aait.ayadenaaprovider.UI.Adapters.ShopCategoriesAdapter;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;
import com.aait.ayadenaaprovider.Uitls.PermissionUtils;
import com.aait.ayadenaaprovider.Uitls.ProgressRequestBody;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.ayadenaaprovider.App.Constant.RequestPermission.REQUEST_IMAGES;

public class EditProductActivity extends ParentActivity implements OnItemClickListener,ProgressRequestBody.UploadCallbacks {
    @BindView(R.id.category_recycler)
    RecyclerView category_recycler;
    ArrayList<ShopCategories> shopCategories = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    ShopCategoriesAdapter shopCategoriesAdapter;
    int id;
    AddAdditionsAdapter addAdditionsAdapter;
    LinearLayoutManager linearLayoutManager1;
    List<AddAdditiveModel> mAdditionModels = new ArrayList<>();
    @BindView(R.id.additions_recycler)
    RecyclerView additions_recycler;
    @BindView(R.id.cir_product_image)
    CircleImageView cir_product_image;
    @BindView(R.id.product_ar)
    EditText product_ar;
    @BindView(R.id.product_en)
    EditText product_en;
    @BindView(R.id.product_desc)
    EditText product_desc;
    @BindView(R.id.product_price)
    EditText product_price;
    @BindView(R.id.add_product)
    Button add_product;
    String ImageBasePath = null;
    ArrayList<Uri> ImageList = new ArrayList<>();
    ArrayList<String> images = new ArrayList<>();
    String additionsList;
    String id1;
    @Override
    protected void initializeComponents() {
        id1 = getIntent().getStringExtra("id");
        add_product.setText(getString(R.string.product_edit));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        shopCategoriesAdapter = new ShopCategoriesAdapter(mContext,shopCategories,R.layout.recycler_category);
        shopCategoriesAdapter.setOnItemClickListener(this);
        category_recycler.setLayoutManager(linearLayoutManager);
        category_recycler.setAdapter(shopCategoriesAdapter);
        mAdditionModels.add(new AddAdditiveModel());
        linearLayoutManager1 = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        addAdditionsAdapter = new AddAdditionsAdapter(mContext, mAdditionModels);
        additions_recycler.setLayoutManager(linearLayoutManager1);
        additions_recycler.setAdapter(addAdditionsAdapter);

        getcats();
        showProduct(mSharedPrefManager.getUserData().getUser_id());
    }
    @OnClick(R.id.add)
    void onAddClick(){
        addAdditionsAdapter.Insert(new AddAdditiveModel());
    }
    void getcats(){
        shopCategories = mSharedPrefManager.getUserData().getShop().getShop_categories();

        shopCategoriesAdapter.updateAll(shopCategories);
    }
    @OnClick(R.id.cir_product_image)
    void onImage(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        for (int i =0;i<ImageList.size();i++){
                            images.add(ImageList.get(i).getPath());
                        }

                        ImageBasePath = ImageList.get(0).getPath();

                        cir_product_image.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(10)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_add_product;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId()==R.id.cat){

            shopCategories.get(position).setChecked(true);


            shopCategories.get(position).setChecked(true);

            id = shopCategories.get(position).getCategory_id();
            Log.e("id",id+"");
        }
    }
    boolean validate(){
         if (product_ar.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.name_ar));
            return false;

        }else if (product_en.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.name_en));
            return false;
        }else if (product_desc.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.prod_description));
            return false;
        }else if (product_price.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.prod_price));
            return false;
        }else if (id==0){
            CommonUtil.makeToast(mContext,getString(R.string.choose_category));
            return false;
        }
        return true;
    }
    void setData(ProductModel mFoodModel) {
        setToolbarTitle(mFoodModel.getProduct_name());
        Glide.with(mContext).load(mFoodModel.getProduct_image()).asBitmap().placeholder(R.mipmap.splash)
                .into(cir_product_image);
        product_ar.setText(mFoodModel.getProduct_nameAr());
        product_en.setText(mFoodModel.getProduct_nameEn());
        product_desc.setText(mFoodModel.getProduct_disc());
        product_price.setText(mFoodModel.getProduct_price() );





    }
    private void showProduct(int user_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).showproduct(user_id,Integer.parseInt(id1)).enqueue(new Callback<ShowProductResponse>() {
            @Override
            public void onResponse(Call<ShowProductResponse> call, Response<ShowProductResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        setData(response.body().getData());
                       // mFoodModel = response.body().getData();
                     //   mSpecialAdditiveAdapter.updateAll(response.body().getData().getProduct_additions());
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ShowProductResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    @OnClick(R.id.add_product)
    void onAddProduct(){
        if (validate()){


            additionsList = new Gson().toJson(mAdditionModels);

            Log.e("add",additionsList);
            if (ImageBasePath==null){
            if (mAdditionModels.get(0).getPrice()==0)
            {
                AddProduct();
            }else {
                AddProduct1(additionsList);
            }}else {
                if (mAdditionModels.get(0).getPrice()==0)
                {
                    AddProduct(images);
                }else {
                    AddProduct(additionsList,images);
                }
            }

        }


    }
    private void AddProduct(String additions,ArrayList<String> paths){

        List<MultipartBody.Part> img = new ArrayList<>();
        showProgressDialog(getString(R.string.please_wait));
        for (String photo :paths) {
            MultipartBody.Part filePart = null;
            File ImageFile = new File(photo);
            ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, EditProductActivity.this);
            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.getName(), fileBody);
            img.add(filePart);
        }
        RetroWeb.getClient().create(ServiceApi.class).updateProduct(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(id1),product_ar.getText().toString(),product_en.getText().toString(),product_desc.getText().toString(),product_price.getText().toString(),id,additions,img).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,FamilyDetailsActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }

    private void AddProduct(ArrayList<String> paths){

        List<MultipartBody.Part> img = new ArrayList<>();
        showProgressDialog(getString(R.string.please_wait));
        for (String photo :paths) {
            MultipartBody.Part filePart = null;
            File ImageFile = new File(photo);
            ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, EditProductActivity.this);
            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.getName(), fileBody);
            img.add(filePart);
        }
        RetroWeb.getClient().create(ServiceApi.class).UpdateProduct(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(id1),product_ar.getText().toString(),product_en.getText().toString(),product_desc.getText().toString(),product_price.getText().toString(),id,img).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,FamilyDetailsActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    private void AddProduct1(String additions){

        showProgressDialog(getString(R.string.please_wait));

        RetroWeb.getClient().create(ServiceApi.class).updateProductwitoutimage(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(id1),product_ar.getText().toString(),product_en.getText().toString(),product_desc.getText().toString(),product_price.getText().toString(),id,additions).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,FamilyDetailsActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }

    private void AddProduct(){

        showProgressDialog(getString(R.string.please_wait));

        RetroWeb.getClient().create(ServiceApi.class).UpdateProductwithoutimage(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(id1),product_ar.getText().toString(),product_en.getText().toString(),product_desc.getText().toString(),product_price.getText().toString(),id).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,FamilyDetailsActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
}
