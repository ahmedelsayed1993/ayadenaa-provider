package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.aait.ayadenaaprovider.App.Constant;
import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.Listeners.OnItemClickListener;
import com.aait.ayadenaaprovider.Models.CategoriesModel;
import com.aait.ayadenaaprovider.Models.CategoriesResponse;
import com.aait.ayadenaaprovider.Models.RegisterResponse;
import com.aait.ayadenaaprovider.Models.UserModel;
import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.UI.Adapters.CategoriesAdapter;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;
import com.aait.ayadenaaprovider.Uitls.ProgressRequestBody;
import com.aait.ayadenaaprovider.Uitls.ValidationUtils;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompleteRegisterActivity extends ParentActivity implements OnItemClickListener,ProgressRequestBody.UploadCallbacks {
    @BindView(R.id.category_recycler)
    RecyclerView category_recycler;
    LinearLayoutManager linearLayoutManager;
    ArrayList<CategoriesModel> categoriesModels = new ArrayList<>();
    CategoriesAdapter categoriesAdapter;
    ArrayList<Integer> ids = new ArrayList<>();
    @BindView(R.id.myRadioGroup)
    RadioGroup myRadioGroup;
    @BindView(R.id.til_store_name)
    TextInputLayout tilStoreName;

    @BindView(R.id.et_store_name)
    TextInputEditText etStoreName;
    @BindView(R.id.radio_yes)
    RadioButton radioYes;

    @BindView(R.id.radio_no)
    RadioButton radioNo;


    @BindView(R.id.til_delivery_phone)
    TextInputLayout tilDeliveryPhone;

    @BindView(R.id.et_delivery_phone)
    TextInputEditText etDeliveryPhone;

    @BindView(R.id.til_delivry_cost)
    TextInputLayout tilDelivryCost;

    @BindView(R.id.et_delivery_cost)
    TextInputEditText etDeliveryCost;

    @BindView(R.id.lay_show_has_delivery)
    LinearLayout layShowHasDelivery;
    UserModel mRegisterSendModel;
    String type;
    String categories;
    boolean isHasDelivery = false;
    public static void startActivity(AppCompatActivity mAppCompatActivity, UserModel registerSendModel,String type) {
        Intent mIntent = new Intent(mAppCompatActivity, CompleteRegisterActivity.class);
        mIntent.putExtra("user", registerSendModel);
        mIntent.putExtra("type",type);
        mAppCompatActivity.startActivity(mIntent);
    }


    void getBundleData() {
        mRegisterSendModel = (UserModel) getIntent().getSerializableExtra("user");
        type = getIntent().getStringExtra("type");
    }

    @Override
    protected void initializeComponents() {
        getBundleData();
        radioYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                isHasDelivery = b;
                if (b) {
                    layShowHasDelivery.setVisibility(View.VISIBLE);
                } else {
                    layShowHasDelivery.setVisibility(View.GONE);
                }
            }
        });

        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        categoriesAdapter = new CategoriesAdapter(mContext,categoriesModels,R.layout.recycler_category);
        categoriesAdapter.setOnItemClickListener(this);
        category_recycler.setLayoutManager(linearLayoutManager);
        category_recycler.setAdapter(categoriesAdapter);
        getCategories();

    }
    @OnClick(R.id.btn_register)
    void onRegister(){
        if (isHasDelivery) {
            if (registerValidateWithDelivery()) {
                registerClientWithImage(mRegisterSendModel.getName(), mRegisterSendModel.getPhone(),
                        mRegisterSendModel.getEmail(), mRegisterSendModel.getCity_id(),
                        mRegisterSendModel.getPassword(), etStoreName.getText().toString(),
                        mRegisterSendModel.getCivil_number(), mRegisterSendModel.getNationality_id(),
                        mRegisterSendModel.getLat(), mRegisterSendModel.getLng(), 1,
                        categories,
                        etDeliveryCost.getText().toString(), etDeliveryPhone.getText().toString(),
                        type,
                        mRegisterSendModel.getAvatar());
            }
        } else {
            if (registerValidation()) {
                registerClientWithImage(mRegisterSendModel.getName(), mRegisterSendModel.getPhone(),
                        mRegisterSendModel.getEmail(), mRegisterSendModel.getCity_id(),
                        mRegisterSendModel.getPassword(), etStoreName.getText().toString(),
                        mRegisterSendModel.getCivil_number(), mRegisterSendModel.getNationality_id(),
                        mRegisterSendModel.getLat(), mRegisterSendModel.getLng(), 0,
                        categories,
                        "", "",
                        type,
                        mRegisterSendModel.getAvatar());
            }
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register_complete;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId()==R.id.cat){

            if (categoriesModels.get(position).isChecked()) {
                categoriesModels.get(position).setChecked(false);
            } else {
                categoriesModels.get(position).setChecked(true);
            }
            categoriesAdapter.notifyDataSetChanged();

            for (CategoriesModel categoryModel : categoriesModels) {
                if (categoryModel.isChecked()) {
                    ids.add(categoryModel.getId());
                }
            }
            int i = 0;

            String id = "";
            for (CategoriesModel categoryModel : categoriesModels) {
                i++;
                if (categoryModel.isChecked()) {
                    if (i != categoriesModels.size()) {
                        id = id+categoryModel.getId()+",";

                    } else {
                        id = id + categoryModel.getId() + "";

                    }
                }
            }
            if (id != null && id.length()>0 && id.endsWith(",")){
                id = id.replace(id.substring(id.length()-1),"");
            }
            categories = id;
            Log.e("id",categories);
        }

    }
    boolean registerValidation() {
        if (!ValidationUtils.checkError(etStoreName, tilStoreName, getString(R.string.fill_empty))) {
            return false;
        } else if (categories.equals("")) {
            CommonUtil.makeToast(mContext,getString(R.string.choose_category));
            return false;
        }
        return true;
    }

    boolean registerValidateWithDelivery() {
        if (!ValidationUtils.checkError(etStoreName, tilStoreName, getString(R.string.fill_empty))) {
            return false;
        } else if (categories.equals("")) {
            CommonUtil.makeToast(mContext,getString(R.string.choose_category));
            return false;
        } else if (!ValidationUtils.checkError(etDeliveryPhone, tilDeliveryPhone, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etDeliveryCost, tilDelivryCost, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }
    private void getCategories(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCategory(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().size()==0){
                            category_recycler.setVisibility(View.GONE);
                        }else {
                            categoriesAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void registerClientWithImage(String username, String mobile, String email, int cityID, String password,
                                         String fullname, String numberCivilRegistry, int nationalityID, String lat, String lng,
                                         int haveMandop,
                                         String Categories, String delegatePrice, String delegatePhone, String providerType,
                                         String pathFromImage) {
        showProgressDialog(getResources().getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(pathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, CompleteRegisterActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class)
                .delegateRegister(mLanguagePrefManager.getAppLanguage(),"2",username, password, email, mobile, cityID+"", lat, lng,
                        nationalityID, numberCivilRegistry, filePart, providerType, fullname, haveMandop, delegatePhone, delegatePrice,Categories
                        )
                .enqueue(new Callback<RegisterResponse>() {
                    @Override
                    public void onResponse(Call<RegisterResponse> call,
                                           Response<RegisterResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()==1) {

                                CommonUtil.makeToast(mContext,response.body().getMsg());
                                ConfirmCodeActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getUser_id()+"");
                            } else {
                                CommonUtil.makeToast(mContext, response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                });
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
