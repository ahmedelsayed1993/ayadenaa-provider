package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaprovider.Base.ParentActivity;


import com.aait.ayadenaaprovider.Fcm.MyFirebaseInstanceIDService;
import com.aait.ayadenaaprovider.Models.LoginResponse;
import com.aait.ayadenaaprovider.Network.RetroWeb;

import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;
import com.aait.ayadenaaprovider.Uitls.ValidationUtils;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud on 2/6/18.
 */

public class LoginActivity extends ParentActivity {


    @BindView(R.id.lay_splash)
    LinearLayout laySplash;

    @BindView(R.id.til_name)
    TextInputLayout tilName;

    @BindView(R.id.et_name)
    TextInputEditText etName;

    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    @BindView(R.id.et_password)
    TextInputEditText etPassword;

    @BindView(R.id.tv_forget_pass)
    TextView tvForgetPass;


    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, LoginActivity.class);
        mIntent.addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }


    @OnClick(R.id.btn_login)
    void onBtnLoginClick() {
        if (loginValidation()) {
          Login();
        }
    }

    @OnClick(R.id.btn_new_account)
    void onBtnNewAccountClick() {
        startActivity(new Intent(mContext,ChooseRegisterActivity.class));
        //RegisterActivity.startActivity((AppCompatActivity) mContext);
    }

    @OnClick(R.id.tv_forget_pass)
    void onForgetPAssClick() {
        ForgetPasswordActivity.startActivity((AppCompatActivity) mContext);
    }


    boolean loginValidation() {
        if (!ValidationUtils.checkError(etName, tilName, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etPassword, tilName, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }
    private void Login(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).Login(mLanguagePrefManager.getAppLanguage(),
                etName.getText().toString(),etPassword.getText().toString(),"else", MyFirebaseInstanceIDService.getToken(mContext)).
                enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()){
                            Log.e("kkkkk",new Gson().toJson(response.body()));

                            if (response.body().getStatus()== 1){

                                mSharedPrefManager.setUserData(response.body().getData());
                                mSharedPrefManager.setLoginStatus(true);
                                MainActivity.startActivity((AppCompatActivity)mContext);
                            }else if (response.body().getStatus() == 2){
                                ConfirmCodeActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getUser_id()+"");
                            }
                            else if (response.body().getStatus() == 0)
                            {    emptyForm();
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                        else {
                            emptyForm();
                            CommonUtil.makeToast(mContext,response.body().getMsg());
                            Log.e("error",response.body().getMsg());
                        }
                    }


                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        Log.e("jjjjjj",new Gson().toJson(t));
                        CommonUtil.handleException(mContext,t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                });
    }





    void emptyForm() {
        etName.setText("");
        etPassword.setText("");
    }

}
