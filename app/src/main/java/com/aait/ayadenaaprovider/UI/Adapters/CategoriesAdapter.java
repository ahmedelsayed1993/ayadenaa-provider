package com.aait.ayadenaaprovider.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.aait.ayadenaaprovider.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaprovider.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaprovider.Models.CategoriesModel;
import com.aait.ayadenaaprovider.Models.ShopCategories;
import com.aait.ayadenaaprovider.R;

import java.util.List;

import butterknife.BindView;

public class CategoriesAdapter extends ParentRecyclerAdapter<CategoriesModel> {
    int count=0;
    public CategoriesAdapter(Context context, List<CategoriesModel> data, int layoutId) {
        super(context, data, layoutId);
    }
    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycler_category, parent, false);
        CategoriesAdapter.ViewHolder holder = new CategoriesAdapter.ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final CategoriesAdapter.ViewHolder viewHolder = (CategoriesAdapter.ViewHolder) holder;
        final CategoriesModel categoryModel = data.get(position);
        viewHolder.check.setText(categoryModel.getName());


        viewHolder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
                if (categoryModel.isChecked()){
                    viewHolder.check.setChecked(true);
                }else {
                    viewHolder.check.setChecked(false);
                }



            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.cat)
        CheckBox check;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
