package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.Listeners.OnItemClickListener;
import com.aait.ayadenaaprovider.Models.NewOrderResponse;
import com.aait.ayadenaaprovider.Models.NewTrackModel;
import com.aait.ayadenaaprovider.Models.NewTracksResponse;
import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.UI.Adapters.NewTracksAdapter;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewTracksActivity extends ParentActivity implements OnItemClickListener {
    NewTracksAdapter newTracksAdapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<NewTrackModel> newTrackModels = new ArrayList<>();
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.track_orders));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        newTracksAdapter = new NewTracksAdapter(mContext,newTrackModels);
        newTracksAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(newTracksAdapter);
        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProccessedOrder(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage());
            }
        });

        getProccessedOrder(mSharedPrefManager.getUserData().getUser_id(),mLanguagePrefManager.getAppLanguage());


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_recycle;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(View view, int position) {
        if (newTrackModels.get(position).getStatus()==0) {
            Intent intent = new Intent(mContext, NewTrackDetailsActivity.class);
            intent.putExtra("id", newTrackModels.get(position).getId() + "");
            startActivity(intent);
        }else if (newTrackModels.get(position).getStatus()==2){

        }else {
            Intent intent = new Intent(mContext, CurrentTrackDetailsActivity.class);
            intent.putExtra("id", newTrackModels.get(position).getId() + "");
            startActivity(intent);
        }


    }
    private void getProccessedOrder(int user_id, String lang) {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getNewTracks(user_id, lang)
                .enqueue(new Callback<NewTracksResponse>() {
                    @Override
                    public void onResponse(Call<NewTracksResponse> call,
                                           Response<NewTracksResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getData().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                newTracksAdapter.updateAll(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<NewTracksResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}
