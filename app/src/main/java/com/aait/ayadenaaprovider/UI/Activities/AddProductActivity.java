package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.aait.ayadenaaprovider.App.Constant;
import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.Listeners.OnItemClickListener;
import com.aait.ayadenaaprovider.Models.AddAdditiveModel;
import com.aait.ayadenaaprovider.Models.BaseResponse;
import com.aait.ayadenaaprovider.Models.ShopCategories;
import com.aait.ayadenaaprovider.Models.ShopCategory;
import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.UI.Adapters.AddAdditionsAdapter;
import com.aait.ayadenaaprovider.UI.Adapters.ShopCategoriesAdapter;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;
import com.aait.ayadenaaprovider.Uitls.PermissionUtils;
import com.aait.ayadenaaprovider.Uitls.ProgressRequestBody;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.ayadenaaprovider.App.Constant.RequestPermission.REQUEST_IMAGES;
import static com.aait.ayadenaaprovider.App.Constant.SearchKeys.price;

public class AddProductActivity extends ParentActivity implements OnItemClickListener,ProgressRequestBody.UploadCallbacks {
    @BindView(R.id.category_recycler)
    RecyclerView category_recycler;
    ArrayList<ShopCategories> shopCategories = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    ShopCategoriesAdapter shopCategoriesAdapter;
    int id;
    AddAdditionsAdapter addAdditionsAdapter;
    LinearLayoutManager linearLayoutManager1;
    List<AddAdditiveModel> mAdditionModels = new ArrayList<>();
    @BindView(R.id.additions_recycler)
    RecyclerView additions_recycler;
    @BindView(R.id.cir_product_image)
    CircleImageView cir_product_image;
    @BindView(R.id.product_ar)
    EditText product_ar;
    @BindView(R.id.product_en)
    EditText product_en;
    @BindView(R.id.product_desc)
    EditText product_desc;
    @BindView(R.id.product_price)
    EditText product_price;
    String ImageBasePath = null;
    ArrayList<Uri> ImageList = new ArrayList<>();
    ArrayList<String> images = new ArrayList<>();
    String additionsList;
    private static final int REQUEST_CODE = 4;
    private static final int REQUEST_CODE_MATISSE = 5;
    private String Type = null;
    @Override
    protected void initializeComponents() {
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        shopCategoriesAdapter = new ShopCategoriesAdapter(mContext,shopCategories,R.layout.recycler_category);
        shopCategoriesAdapter.setOnItemClickListener(this);
        category_recycler.setLayoutManager(linearLayoutManager);
        category_recycler.setAdapter(shopCategoriesAdapter);
        mAdditionModels.add(new AddAdditiveModel());
        linearLayoutManager1 = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        addAdditionsAdapter = new AddAdditionsAdapter(mContext, mAdditionModels);
        additions_recycler.setLayoutManager(linearLayoutManager1);
        additions_recycler.setAdapter(addAdditionsAdapter);

        getcats();

    }
    @OnClick(R.id.add)
    void onAddClick(){
        addAdditionsAdapter.Insert(new AddAdditiveModel());
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_add_product;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId()==R.id.cat){

            shopCategories.get(position).setChecked(true);


                shopCategories.get(position).setChecked(true);

            id = shopCategories.get(position).getCategory_id();
            Log.e("id",id+"");
        }

    }
    boolean validate(){
        if (ImageBasePath == null){
            CommonUtil.makeToast(mContext,getString(R.string.choose_image));
            return false;
        }else if (product_ar.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.name_ar));
            return false;

        }else if (product_en.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.name_en));
            return false;
        }else if (product_desc.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.prod_description));
            return false;
        }else if (product_price.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.prod_price));
            return false;
        }else if (id==0){
            CommonUtil.makeToast(mContext,getString(R.string.choose_category));
            return false;
        }
        return true;
    }
    void getcats(){
        shopCategories = mSharedPrefManager.getUserData().getShop().getShop_categories();

        shopCategoriesAdapter.updateAll(shopCategories);
    }
    @OnClick(R.id.cir_product_image)
    void onImage(){
//        getPickImageWithPermission();
        getFile();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        for (int i =0;i<ImageList.size();i++){
                            images.add(ImageList.get(i).getPath());
                        }

                        ImageBasePath = ImageList.get(0).getPath();

                         cir_product_image.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(10)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }
    @OnClick(R.id.add_product)
    void onAddProduct(){
        if (validate()){


            additionsList = new Gson().toJson(mAdditionModels);

            Log.e("add",additionsList);
            if (mAdditionModels.get(0).getPrice()==0)
            {
                AddProduct(images);
            }else {
                AddProduct(additionsList, images);
            }

        }


    }
    private void getFile() {
        if (ActivityCompat.checkSelfPermission(AddProductActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE);
        } else {
            attach();
        }
    }
    private void attach() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select file to upload "), REQUEST_CODE_MATISSE);

    }
    private void AddProduct(String additions,ArrayList<String> paths){

        List<MultipartBody.Part> img = new ArrayList<>();
        showProgressDialog(getString(R.string.please_wait));
        for (String photo :paths) {
            MultipartBody.Part filePart = null;
            File ImageFile = new File(photo);
            ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, AddProductActivity.this);
            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.getName(), fileBody);
            img.add(filePart);
        }
        RetroWeb.getClient().create(ServiceApi.class).addProduct(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),product_ar.getText().toString(),product_en.getText().toString(),product_desc.getText().toString(),product_price.getText().toString(),id,additions,img).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,FamilyDetailsActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }

    private void AddProduct(ArrayList<String> paths){

        List<MultipartBody.Part> img = new ArrayList<>();
        showProgressDialog(getString(R.string.please_wait));
        for (String photo :paths) {
            MultipartBody.Part filePart = null;
            File ImageFile = new File(photo);
            ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, AddProductActivity.this);
            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.getName(), fileBody);
            img.add(filePart);
        }
        RetroWeb.getClient().create(ServiceApi.class).AddProduct(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),product_ar.getText().toString(),product_en.getText().toString(),product_desc.getText().toString(),product_price.getText().toString(),id,img).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,FamilyDetailsActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
             if (requestCode == REQUEST_CODE_MATISSE) {
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    Log.e("path", uri.toString());
                    String path = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        path = CommonUtil.getPath(this, uri);
                        Log.e("path", path);

                        Type = getContentResolver().getType(uri);
                        Log.e("type",Type);
                        if ((Type.startsWith("image"))){
                            ImageBasePath = CommonUtil.getPath(AddProductActivity.this,uri);
                           images.add(ImageBasePath);
//                            for (int i =0;i<ImageList.size();i++){
//                                images.add(ImageList.get(i).getPath());
//                            }
                            Log.e("image",new Gson().toJson(images));

                            cir_product_image.setImageURI(Uri.parse(ImageBasePath));

                        }else {

                        }
                    }

                }

            }
        }
    }
}
