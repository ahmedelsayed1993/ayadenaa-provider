package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.TextView;

import com.aait.ayadenaaprovider.App.Constant;
import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.Models.BaseResponse;
import com.aait.ayadenaaprovider.Models.NewTrackModel;
import com.aait.ayadenaaprovider.Models.NewTrackResponse;
import com.aait.ayadenaaprovider.Models.OrderModel;
import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewTrackDetailsActivity extends ParentActivity {
    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;



    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;


    @BindView(R.id.iv_client_image)
    CircleImageView ivClientImage;

    @BindView(R.id.tv_client_name)
    TextView tvClientName;

    @BindView(R.id.tv_client_city)
    TextView tvClientCity;
    @BindView(R.id.tv_total_coast)
    TextView tv_total_coast;



    @BindView(R.id.btn_accept)
    Button btn_accept;
    @BindView(R.id.btn_cancel)
    Button btn_accept1;
    @BindView(R.id.tv_price)
    TextView tv_price;

   String id;
    @Override
    protected void initializeComponents() {
        id = getIntent().getStringExtra("id");
        getTrack();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_new_track_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    void setOrderData(NewTrackModel orderData) {

        Glide.with(mContext).load(orderData.getUser_image()).asBitmap()
                .into(ivClientImage);

        tvOrderTime
                .setText(orderData.getCreated_at() + " ");
        tvOrderNumber
                .setText(orderData.getId() + "");

        tvClientName.setText(orderData.getUser_name());


        tvClientCity.setText(orderData.getAddress());
        tv_total_coast.setText(orderData.getPrice()+" "+mContext.getResources().getString(R.string.SAR));
        tv_price.setText(orderData.getPersons()+"");

    }
    private void getTrack(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).showTrack(mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(id)).enqueue(new Callback<NewTrackResponse>() {
            @Override
            public void onResponse(Call<NewTrackResponse> call, Response<NewTrackResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        setOrderData(response.body().getData());
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<NewTrackResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.btn_accept)
    void onAccept(){
        changeStatus(1,getString(R.string.accept_track));
    }
    @OnClick(R.id.btn_cancel)
    void onCancel(){
        changeStatus(0,getString(R.string.refuse_track));
    }
    private void changeStatus(int status, final String message){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeTrack(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(id),status).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,message);
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
