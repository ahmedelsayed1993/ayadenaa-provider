package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.Base.ParentActivity;

import butterknife.OnClick;

/**
 * Created by Mahmoud on 3/9/18.
 */

public class ProfileStatusActivity extends ParentActivity {


    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, ProfileStatusActivity.class);
        mAppCompatActivity.startActivity(mIntent);
    }

    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile_status;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }


    @OnClick(R.id.btn_profile_data)
    void onBtnProfileDataClick() {
        startActivity(new Intent(mContext,ProfileActivity.class));
        //ProfileActivity.startActivity((AppCompatActivity) mContext);
    }

    @OnClick(R.id.btn_store_data)
    void onBtnStoreDataClick() {
        //StoreProfileActivity.startActivity((AppCompatActivity) mContext);
        startActivity(new Intent(mContext,StoreProfileActivity.class));
    }
}
