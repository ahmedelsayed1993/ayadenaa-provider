package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.Listeners.DrawerListner;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.UI.Fragments.NavigationFragment;

import com.aait.ayadenaaprovider.UI.Fragments.NewOrderFragment;
import com.aait.ayadenaaprovider.UI.Fragments.ProcessedOrderFragment;
import com.aait.ayadenaaprovider.UI.Fragments.StoreFragment;
import com.aait.ayadenaaprovider.UI.Views.BottomNavigationViewHelper;

import java.util.Stack;

import butterknife.BindView;
import butterknife.OnClick;


public class MainActivity extends ParentActivity
        implements DrawerListner, BottomNavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemReselectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.nav_view)
    FrameLayout navView;

    @BindView(R.id.app_bar)
    AppBarLayout app_bar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv_menu)
    ImageView ivMenu;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;

    NavigationFragment mNavigationFragment;

    NewOrderFragment mHomeFragment;
////
   StoreFragment mOrdersFragment;
////
    ProcessedOrderFragment mOffersFragment;


    int selectedTab = 0;

    private Stack<Integer> tabsStack = new Stack<>();

    private FragmentManager fragmentManager;

    private FragmentTransaction transaction;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, MainActivity.class);
        mAppCompatActivity.startActivity(mIntent);
    }

    @OnClick(R.id.iv_menu)
    void onMenuClick() {
        this.OpenCloseDrawer();
    }




    @Override
    public void OpenCloseDrawer() {
        mNavigationFragment.setNavData();
        if (drawerLayout != null) {
            if (mLanguagePrefManager.getAppLanguage().equals("en")) {
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            } else {
                if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        }
    }


    @Override
    protected void initializeComponents() {
        mNavigationFragment = NavigationFragment.newInstance();
        mNavigationFragment.setDrawerListner(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_view, mNavigationFragment).commit();
        initializeBottomNav();
        BottomNavigationViewHelper.disableShiftMode(bottomNavigation);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onNavigationItemReselected(@NonNull final MenuItem item) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                showHome(true);
                break;
            case R.id.navigation_order:
                showOrders(true);
                break;
            case R.id.navigation_offers:
                showOffers(true);
                break;
        }
        return true;
    }

    public void initializeBottomNav() {

       mHomeFragment = NewOrderFragment.newInstance();
        mOrdersFragment = StoreFragment.newInstance();
        mOffersFragment = ProcessedOrderFragment.newInstance();
////

        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
       transaction.add(R.id.home_fragment_container, mHomeFragment);
        transaction.add(R.id.home_fragment_container, mOrdersFragment);
        transaction.add(R.id.home_fragment_container, mOffersFragment);
        transaction.commit();
        bottomNavigation.setOnNavigationItemSelectedListener(this);
        bottomNavigation.setOnNavigationItemReselectedListener(this);
        bottomNavigation.setSelectedItemId(R.id.navigation_home);
        showHome(true);
    }


    private void showOffers(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
        transaction.hide(mHomeFragment);
        transaction.hide(mOrdersFragment);
       transaction.show(mOffersFragment);
        transaction.commit();
        selectedTab = R.id.navigation_offers;

        tvTitle.setText(R.string.offers);
        showParElevation(true);

    }

    private void showOrders(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
      transaction.hide(mOffersFragment);
        transaction.hide(mHomeFragment);
        transaction.show(mOrdersFragment);
        transaction.commit();
        selectedTab = R.id.navigation_order;
        tvTitle.setText(R.string.store);
        showParElevation(true);

    }

    private void showHome(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
         transaction.hide(mOrdersFragment);
       transaction.hide(mOffersFragment);
        transaction.show(mHomeFragment);
        transaction.commit();
        selectedTab = R.id.navigation_home;

        tvTitle.setText(R.string.home);
        showParElevation(false);
    }

    public void showParElevation(boolean showHide) {
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {

            if (showHide) {
                app_bar.setElevation((float) 5.0);

            } else {
                app_bar.setElevation((float) 0.0);
            }
        }

    }

    @Override
    public void onBackPressed() {
        if (tabsStack.size() > 0) {
            bottomNavigation.setOnNavigationItemSelectedListener(null);
            int selectedTab = tabsStack.pop();
            bottomNavigation.setSelectedItemId(selectedTab);
            switch (selectedTab) {
                case R.id.navigation_home:
                    showHome(false);
                    break;
                case R.id.navigation_order:
                    showOrders(false);
                    break;
                case R.id.navigation_offers:
                    showOffers(false);
                    break;
            }
            bottomNavigation.setOnNavigationItemSelectedListener(this);
        } else {
            MainActivity.this.finish();
        }
    }



}
