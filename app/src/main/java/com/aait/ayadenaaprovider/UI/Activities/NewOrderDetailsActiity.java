package com.aait.ayadenaaprovider.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.ayadenaaprovider.App.Constant;
import com.aait.ayadenaaprovider.Base.ParentActivity;
import com.aait.ayadenaaprovider.Models.BaseResponse;
import com.aait.ayadenaaprovider.Models.OrderModel;
import com.aait.ayadenaaprovider.Models.OrderProductModel;
import com.aait.ayadenaaprovider.Network.RetroWeb;
import com.aait.ayadenaaprovider.Network.ServiceApi;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.UI.Adapters.OrderDetailsAdapter;
import com.aait.ayadenaaprovider.Uitls.CommonUtil;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud on 3/10/18.
 */

public class NewOrderDetailsActiity extends ParentActivity {

    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;



    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;


    @BindView(R.id.iv_client_image)
    CircleImageView ivClientImage;

    @BindView(R.id.tv_client_name)
    TextView tvClientName;

    @BindView(R.id.tv_client_city)
    TextView tvClientCity;
    @BindView(R.id.tv_total_coast)
    TextView tv_total_coast;

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.btn_accept)
    Button btn_accept;
    @BindView(R.id.btn_cancel)
            Button btn_accept1;



    LinearLayoutManager linearLayoutManager;

    OrderDetailsAdapter mFamilyOrderDetailsAdapter;

    List<OrderProductModel> mFamilyOrderModels = new ArrayList<>();

    OrderModel mOrderModel;

    public static void startActivity(AppCompatActivity mAppCompatActivity, OrderModel orderModel) {
        Intent mIntent = new Intent(mAppCompatActivity, NewOrderDetailsActiity.class);
        mIntent.putExtra(Constant.BundleData.ORDER, orderModel);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData() {
        mOrderModel = (OrderModel) getIntent().getSerializableExtra(Constant.BundleData.ORDER);
        setOrderData(mOrderModel);
    }

    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.order_details));


        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mFamilyOrderDetailsAdapter = new OrderDetailsAdapter(mContext, mFamilyOrderModels,
                R.layout.recycle_family_order_details);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(mFamilyOrderDetailsAdapter);
        getBundleData();



//        getOrdersDetails(
//                "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOi8vZWxzaGVyYmVueS5hcmFic2Rlc2lnbi5jb20vdjMvYXlhZGVuYS9hcGkvYXV0aC9zaWduaW4iLCJpYXQiOjE1MjI1ODIxNTIsImV4cCI6MTY3ODEwMjE1MiwibmJmIjoxNTIyNTgyMTUyLCJqdGkiOiJKNWpVZzliWVFnMFlibjJpIn0.CDEYfz7ycXk_Nuk2Lyc43FcikZifjuwc_8o8FlGekZA",
//                mLanguagePrefManager.getAppLanguage(), mOrderModel.getId());
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_new_order_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    void setOrderData(OrderModel orderData) {

        Glide.with(mContext).load(orderData.getOrder_user_avatar()).asBitmap()
                .into(ivClientImage);

        tvOrderTime
                .setText(orderData.getOrder_created_at() + " ");
        tvOrderNumber
                .setText(orderData.getOrder_id() + "");

        tvClientName.setText(orderData.getOrder_user_name());


        tvClientCity.setText(orderData.getOrder_user_city());
        tv_total_coast.setText(orderData.getTotal_price()+" "+mContext.getResources().getString(R.string.SAR));
        mFamilyOrderDetailsAdapter.updateAll(orderData.getOrder_products());




    }

    @OnClick(R.id.btn_accept)
    void onBtnAcceptClick() {

        change(mOrderModel.getOrder_id());
    }
    @OnClick(R.id.btn_cancel)
    void onOnWayClick(){
        change1(mOrderModel.getOrder_id());
    }


    private void change(int order_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeStatus(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),order_id,1).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext, MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void change1(int order_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeStatus(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),order_id,2).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext, MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void change2(int order_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeStatus(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),order_id,7).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.client_receive_order));
                        startActivity(new Intent(mContext, MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
