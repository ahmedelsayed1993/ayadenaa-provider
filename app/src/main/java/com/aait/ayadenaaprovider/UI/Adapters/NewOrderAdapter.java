package com.aait.ayadenaaprovider.UI.Adapters;


import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaprovider.App.Constant;
import com.aait.ayadenaaprovider.R;
import com.aait.ayadenaaprovider.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaprovider.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaprovider.Models.OrderModel;
import com.bumptech.glide.Glide;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Mahmoud ibrahim on 12/2/2018.
 */

public class NewOrderAdapter extends ParentRecyclerAdapter<OrderModel> {

    private String errorMsg;

    public NewOrderAdapter(final Context context, final List<OrderModel> data) {
        super(context, data);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        switch (viewType) {
            case Constant.InfinitScroll.ITEM:
                View viewItem = inflater.inflate(R.layout.recycle_order_row, parent, false);
                viewHolder = new NewOrderViewHolder(viewItem);
                break;
            case Constant.InfinitScroll.LOADING:
                View viewLoading = inflater.inflate(R.layout.recycle_item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case Constant.InfinitScroll.ITEM:
                NewOrderViewHolder newOrderViewHolder = (NewOrderViewHolder) holder;
                OrderModel orderModel = data.get(position);
                Glide.with(mcontext).load(orderModel.getOrder_user_avatar()).asBitmap()
                        .into(newOrderViewHolder.ivUserImage);
                newOrderViewHolder.tvUserName.setText(orderModel.getOrder_user_name());
                newOrderViewHolder.tvCity.setText(orderModel.getOrder_user_city());
                newOrderViewHolder.tvOrderNumber.setText(orderModel.getOrder_id() + "");
                newOrderViewHolder.tvOrderTime.setText(mcontext.getResources().getString(R.string.order_time)+" "+orderModel.getOrder_created_at());

                break;
            case Constant.InfinitScroll.LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);
                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    mcontext.getString(R.string.error_msg_unknown));
                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == data.size() - 1 && isLoadingAdded)
                ? Constant.InfinitScroll.LOADING
                : Constant.InfinitScroll.ITEM;
    }


    /**
     * Main list's content ViewHolder
     */

    protected class NewOrderViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.iv_user_image)
        CircleImageView ivUserImage;

        @BindView(R.id.tv_order_time)
        TextView tvOrderTime;

        @BindView(R.id.tv_user_name)
        TextView tvUserName;

        @BindView(R.id.tv_city)
        TextView tvCity;

        @BindView(R.id.tv_order_number)
        TextView tvOrderNumber;




        public NewOrderViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);

            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(final View view) {
                    itemClickListener.onItemClick(view, getPosition());
                }
            });
        }


    }


    protected class LoadingVH extends ParentRecyclerViewHolder {


        @BindView(R.id.progress_wheel)
        ProgressWheel mProgressBar;

        @BindView(R.id.loadmore_retry)
        ImageButton mRetryBtn;

        @BindView(R.id.loadmore_errortxt)
        TextView mErrorTxt;

        @BindView(R.id.loadmore_errorlayout)
        LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            ProgressWheel mProgressBar = (ProgressWheel) findViewById(R.id.progress_wheel);
        }

        @OnClick(R.id.loadmore_retry)
        void onLoadMoreRetryClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }

        @OnClick(R.id.loadmore_errorlayout)
        void onLoadMoreErrorLayoutClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }
    }


    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(data.size() - 1);
        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }


}
