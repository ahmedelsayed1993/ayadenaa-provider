package com.aait.ayadenaaprovider.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.ayadenaaprovider.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaprovider.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaprovider.Models.ShopCategories;
import com.aait.ayadenaaprovider.Models.ShopCategory;
import com.aait.ayadenaaprovider.R;

import java.util.List;

import butterknife.BindView;

public class ShopCategoriesAdapter extends ParentRecyclerAdapter<ShopCategories> {
    int count = 0;
    public ShopCategoriesAdapter(Context context, List<ShopCategories> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycler_category, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        final ShopCategories categoryModel = data.get(position);
        viewHolder.check.setText(categoryModel.getCategory_name());


      viewHolder.check.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              itemClickListener.onItemClick(v,position);
              count=1;
              if (count==1){
                  viewHolder.check.setChecked(true);
                  count=0;
              }else {
                  viewHolder.check.setChecked(false);
              }


          }
      });



    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.cat)
        CheckBox check;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
