package com.aait.ayadenaaprovider.Listeners;

import android.view.View;

public interface OnItemClickListener {
    public void onItemClick(View view, int position);
}
