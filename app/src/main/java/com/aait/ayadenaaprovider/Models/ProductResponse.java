package com.aait.ayadenaaprovider.Models;

public class ProductResponse extends BaseResponse
{
    private ProductsResponse data;

    public ProductsResponse getData() {
        return data;
    }

    public void setData(ProductsResponse data) {
        this.data = data;
    }
}
