package com.aait.ayadenaaprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductsResponse implements Serializable {
    private ArrayList<ShopProductModel> products;

    public ArrayList<ShopProductModel> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<ShopProductModel> products) {
        this.products = products;
    }
}
