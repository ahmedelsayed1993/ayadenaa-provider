package com.aait.ayadenaaprovider.Models;

public class NewTrackResponse extends BaseResponse {
    private NewTrackModel data;

    public NewTrackModel getData() {
        return data;
    }

    public void setData(NewTrackModel data) {
        this.data = data;
    }
}
