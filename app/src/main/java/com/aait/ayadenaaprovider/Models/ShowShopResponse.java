package com.aait.ayadenaaprovider.Models;

public class ShowShopResponse extends BaseResponse {
    private ShowShopModel data;

    public ShowShopModel getData() {
        return data;
    }

    public void setData(ShowShopModel data) {
        this.data = data;
    }
}
