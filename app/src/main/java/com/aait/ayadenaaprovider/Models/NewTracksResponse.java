package com.aait.ayadenaaprovider.Models;

import java.util.ArrayList;

public class NewTracksResponse extends BaseResponse {
    private ArrayList<NewTrackModel> data;

    public ArrayList<NewTrackModel> getData() {
        return data;
    }

    public void setData(ArrayList<NewTrackModel> data) {
        this.data = data;
    }
}
