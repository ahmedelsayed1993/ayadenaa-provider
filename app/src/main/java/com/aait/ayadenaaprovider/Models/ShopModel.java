package com.aait.ayadenaaprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ShopModel implements Serializable {
    private int shop_id;
    private String shop_name;
    private String shop_have_delegate;
    private String shop_delegate_phone;
    private String shop_delegate_price;
    private String shop_minimum_person;
    private String shop_active;
    private String shop_person_price;
    private String shop_type;
    private ArrayList<ShopCategories> shop_categories;

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_have_delegate() {
        return shop_have_delegate;
    }

    public void setShop_have_delegate(String shop_have_delegate) {
        this.shop_have_delegate = shop_have_delegate;
    }

    public String getShop_delegate_phone() {
        return shop_delegate_phone;
    }

    public void setShop_delegate_phone(String shop_delegate_phone) {
        this.shop_delegate_phone = shop_delegate_phone;
    }

    public String getShop_delegate_price() {
        return shop_delegate_price;
    }

    public void setShop_delegate_price(String shop_delegate_price) {
        this.shop_delegate_price = shop_delegate_price;
    }

    public String getShop_minimum_person() {
        return shop_minimum_person;
    }

    public void setShop_minimum_person(String shop_minimum_person) {
        this.shop_minimum_person = shop_minimum_person;
    }

    public String getShop_active() {
        return shop_active;
    }

    public void setShop_active(String shop_active) {
        this.shop_active = shop_active;
    }

    public String getShop_person_price() {
        return shop_person_price;
    }

    public void setShop_person_price(String shop_person_price) {
        this.shop_person_price = shop_person_price;
    }

    public String getShop_type() {
        return shop_type;
    }

    public void setShop_type(String shop_type) {
        this.shop_type = shop_type;
    }

    public ArrayList<ShopCategories> getShop_categories() {
        return shop_categories;
    }

    public void setShop_categories(ArrayList<ShopCategories> shop_categories) {
        this.shop_categories = shop_categories;
    }
}
