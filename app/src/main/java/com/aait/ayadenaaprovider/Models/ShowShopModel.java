package com.aait.ayadenaaprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ShowShopModel implements Serializable {
    private int shop_id;
    private String shop_name;
    private String shop_type;
    private boolean shop_have_delegate;
    private String shop_delegate_phone;
    private float shop_delegate_price;
    private String shop_active;
    private String shop_minimum_person;
    private String shop_person_price;
    private float shop_rate;
    private int user_id;
    private String user_name;
    private String user_image;
    private String user_city;
    private ArrayList<ShopCategory> user_categories;
    private ArrayList<ShopProductModel> products;


    public ArrayList<ShopCategory> getUser_categories() {
        return user_categories;
    }

    public void setUser_categories(ArrayList<ShopCategory> user_categories) {
        this.user_categories = user_categories;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_type() {
        return shop_type;
    }

    public void setShop_type(String shop_type) {
        this.shop_type = shop_type;
    }

    public boolean isShop_have_delegate() {
        return shop_have_delegate;
    }

    public void setShop_have_delegate(boolean shop_have_delegate) {
        this.shop_have_delegate = shop_have_delegate;
    }

    public String getShop_delegate_phone() {
        return shop_delegate_phone;
    }

    public void setShop_delegate_phone(String shop_delegate_phone) {
        this.shop_delegate_phone = shop_delegate_phone;
    }

    public float getShop_delegate_price() {
        return shop_delegate_price;
    }

    public void setShop_delegate_price(float shop_delegate_price) {
        this.shop_delegate_price = shop_delegate_price;
    }

    public String getShop_active() {
        return shop_active;
    }

    public void setShop_active(String shop_active) {
        this.shop_active = shop_active;
    }

    public String getShop_minimum_person() {
        return shop_minimum_person;
    }

    public void setShop_minimum_person(String shop_minimum_person) {
        this.shop_minimum_person = shop_minimum_person;
    }

    public String getShop_person_price() {
        return shop_person_price;
    }

    public void setShop_person_price(String shop_person_price) {
        this.shop_person_price = shop_person_price;
    }

    public float getShop_rate() {
        return shop_rate;
    }

    public void setShop_rate(float shop_rate) {
        this.shop_rate = shop_rate;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_city() {
        return user_city;
    }

    public void setUser_city(String user_city) {
        this.user_city = user_city;
    }
}
