package com.aait.ayadenaaprovider.Models;

public class OrderDetailsResponse extends BaseResponse {
    private OrderModel data;

    public OrderModel getData() {
        return data;
    }

    public void setData(OrderModel data) {
        this.data = data;
    }
}
