package com.aait.ayadenaaprovider.Models;

import java.io.Serializable;

public class ShopProductModel implements Serializable {
    private int product_id;
    private String product_name;
    private String product_price;
    private String product_offer;
    private String product_disc;
    private int product_category_id;
    private boolean product_have_offer;
    private String product_image;

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_offer() {
        return product_offer;
    }

    public void setProduct_offer(String product_offer) {
        this.product_offer = product_offer;
    }

    public String getProduct_disc() {
        return product_disc;
    }

    public void setProduct_disc(String product_disc) {
        this.product_disc = product_disc;
    }

    public int getProduct_category_id() {
        return product_category_id;
    }

    public void setProduct_category_id(int product_category_id) {
        this.product_category_id = product_category_id;
    }

    public boolean isProduct_have_offer() {
        return product_have_offer;
    }

    public void setProduct_have_offer(boolean product_have_offer) {
        this.product_have_offer = product_have_offer;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }
}
