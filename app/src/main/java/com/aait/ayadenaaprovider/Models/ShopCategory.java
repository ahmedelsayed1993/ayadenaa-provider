package com.aait.ayadenaaprovider.Models;

import java.io.Serializable;

public class ShopCategory implements Serializable {
    private int category_id;
    private String category_name;
    private boolean isChecked = false;

    private boolean checkCategory = false;

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isCheckCategory() {
        return checkCategory;
    }

    public void setCheckCategory(boolean checkCategory) {
        this.checkCategory = checkCategory;
    }
}
