package com.aait.ayadenaaprovider.Models;

import java.util.ArrayList;

public class RatesResponse extends BaseResponse {
    private ArrayList<RatesModel> data;

    public ArrayList<RatesModel> getData() {
        return data;
    }

    public void setData(ArrayList<RatesModel> data) {
        this.data = data;
    }
}
