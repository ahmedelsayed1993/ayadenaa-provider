package com.aait.ayadenaaprovider.Models;

public class NewPasswordResponse extends BaseResponse {
    private NewPassModel data;

    public NewPassModel getData() {
        return data;
    }

    public void setData(NewPassModel data) {
        this.data = data;
    }
}
